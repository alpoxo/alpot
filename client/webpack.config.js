const webpack = require("webpack");
const { CheckerPlugin } = require("awesome-typescript-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const DashboardPlugin = require("webpack-dashboard/plugin");
var BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;
const path = require("path");

module.exports = {
    entry: [
        "react-hot-loader/patch",
        // activate HMR for React

        "./src/index.tsx",
    ],
    output: {
        path: path.join(__dirname, "dist"),
        filename: "bundle.js",
    },

    // Enable sourcemaps for debugging webpack"s output.
    devtool: "source-map",

    resolve: {
        extensions: [".webpack.js", ".web.js", ".js", ".jsx", ".ts", ".tsx"],
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new CheckerPlugin(),
        new DashboardPlugin(),
        new BundleAnalyzerPlugin(),
        new HtmlWebpackPlugin({
            template: "./index.html",
        }),
    ],

    module: {
        rules: [
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ["file-loader"],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: ["file-loader"],
            },
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: "awesome-typescript-loader",
                        options: { transpileOnly: true },
                    },
                    "source-map-loader",
                    "react-hot-loader/webpack",
                ],
                exclude: /node_modules/,
            },
        ],
    },

    devServer: {
        hot: true,
        publicPath: "/",
        historyApiFallback: true,
    },
};
