import { isNil, isEmpty, flowRight } from "lodash";
import { map, reject, always, apply, curryN } from "lodash/fp";
import defaults from "../validationDefaults";

/**
 * A type which specifies one or multiple transformer functions
 * for a validation message
 */
type MessageTransformer =
    | Array<(input: string, opts: {}) => string>
    | ((input: string, opts: {}) => string);

/**
 * An interface defining options for the
 * validation process. It can be used
 * to define defaults
 * 
 * @export
 * @interface ValidationOptions
 */
export interface ValidationOptions {
    messageTransforms?: MessageTransformer;
    messages?: {
        isRequired?: string;
        length?: string;
        passwordComplexity?: string;
        email?: string;
    };
}

/**
 * Transforms messages with one or more transformer functions
 * 
 * @param {MessageTransformer} transformer - 
 *      A transformer function, or an array of transformer functions
 */
type transformMessage = (
    transformer: MessageTransformer,
) => (input: string, opts: {}) => string;
const transformMessage: transformMessage = transformer => (input, opts) => {
    if (Array.isArray(transformer))
        return flowRight(...transformer)(input, opts);

    return transformer(input, opts);
};

/**
 * Defines the layout of a function which can act as a validator.
 * This is the kind of function which can be passed 
 * to the function `v`
 */
export type ValidationFunction<T> = (input: T, object: any) => string;

/**
 * A validator defines the layout of a validation definition object, 
 * the kind of object given to `validate`.
 */
export type Validator<T> = {
    [K in keyof T]?: ((_: T[K], obj: any) => string[]) | Validator<T[K]>
};

/**
 * The kind of object which is returned by `validate`. It is the final
 * validation response
 */
export type Validated<T> = { [K in keyof T]?: string[] };

/**
 * A curried function with the signature
 *     ( defaultMessage: string, transforms?: MessageTransformer ) => 
 *         <T>(isInvalid: (toValidate: T, validationOptions?: any) => boolean) => 
 *         (options?: any) => 
 *         ValidationFunction<T>
 * 
 * It can be used to define custom validation functions:
 * 
 * @example
 *     export const validateLength = createValidation(
 *       defaults.messages.length,
 *       defaults.messageTransforms,
 *    )(
 *        (input: string, opts: { length: number }) =>
 *           input && input.length && input.length < opts.length,
 *    );
 */
export type createValidation = (
    defaultMessage: string,
    transforms?: MessageTransformer,
) => <T>(
    isInvalid: (
        toValidate: T,
        validateObject: any,
        validationOptions?: any,
    ) => boolean,
) => (options?: any) => ValidationFunction<T>;
export const createValidation: createValidation = (
    defaultMessage,
    transforms,
) => isInvalid => options => (input, object) => {
    let message = defaultMessage;

    if (typeof options === "string") message = options;
    else if (options && options.message) message = options.message;

    if (!isInvalid(input, object, options)) return undefined;

    if (transforms) message = transformMessage(transforms)(message, options);

    return message;
};

/**
 * A curried function for defining a custom validation
 * It can be used to shortly define a new validation function
 * 
 * @example
 * 
 *     const validateForm = validate({
 *      username: v(
 *         withValidation(input => !input)(
 *             "This field is required!"
 *         )
 *      )
 *     })
 */
export const withValidation = createValidation(
    "This field is invalid!",
    defaults.messageTransforms,
);

/**
 * A function which takes validation functions
 * as parameter which were previously created
 * by a call to createValidation or withValidation.
 * 
 * @example
 * 
 *    const validateForm = validate({
 *        username: v(isRequired()),
 *        password: v(
 *            isRequired(),
 *            length({
 *                length: 6,
 *            }),
 *            passwordComplexity(),
 *        ),
 *        email: v(isRequired(), email()),
 *    });
 */
export type v = <T>(
    ...funcs: ValidationFunction<T>[]
) => (input: string, object: any) => string[] | undefined;
export const v: v = (...funcs) => (input, object) =>
    flowRight(
        (result: string[]) => (isEmpty(result) ? undefined : result),
        reject(isNil),
        map((func: Function) => func(input, object)),
    )(funcs);

/**
 * Creates a function which validates all
 * declared fields given. It behaves very
 * much like ramda's `evolve` function except
 * that it does not ignore properties which are
 * not defined on the object.
 * 
 * The returned object of the validation defines
 * only the properties which were invalid. The
 * associated value is an array of failure messages 
 * (strings)
 * 
 * @example
 * 
 *    const validateForm = validate({
 *        username: v(isRequired()),
 *        password: v(
 *            isRequired(),
 *            length({
 *                length: 6,
 *            })
 *        ),
 *        email: v(isRequired(), email()),
 *    });
 * 
 *     const validated = validateForm({
 *         username: "foo",
 *         password: "",
 *         email: "anymail"
 *     });
 *     
 *     console.log(validated);
 *     // Output: {
 *     //     password: [<required failure msg>, <length failure msg>],
 *     //     email: [<mail format failure msg>]
 *     // }
 * 
 * @param {Validator<X extends T & object>} - The property validation object
 * @param {T extends { [ key: string ]: any }} - (curried) The object to validate
 * @returns {Validated<T>} - The final validation response
 */
export const validate = curryN(3, function<
    T extends { [key: string]: any },
    X extends T & object
>(transformations: Validator<X>, object: T): Validated<T> {
    const result: Validated<T> = {};

    for (const key in transformations) {
        const transformation = transformations[key];
        result[key] =
            typeof transformation === "function"
                ? transformation(object && object[key], object)
                : transformation && typeof transformation === "object"
                  ? validate(transformation, object && object[key], object)
                  : object[key];
    }
    return result;
});

/**
 * Defines a property as required
 */
export const isRequired = createValidation(
    defaults.messages.isRequired,
    defaults.messageTransforms,
)(input => !input);

/**
 * Constraints the length of a property (string) to a specific length
 */
export const length = createValidation(
    defaults.messages.length,
    defaults.messageTransforms,
)(
    (input: string, object: any, opts: { length: number }) =>
        input && input.length && input.length < opts.length,
);

/**
 * Constraints the password to a certain complexity
 */
export const passwordComplexity = createValidation(
    defaults.messages.passwordComplexity,
    defaults.messageTransforms,
)(
    (input: string, object: any, opts?: { regex?: RegExp }) =>
        input && !(opts && opts.regex ? opts.regex : /[^a-z]/).test(input),
);

/**
 * Makes the input field to have to match the input
 * of another input field
 */
export const match = createValidation(
    defaults.messages.match,
    defaults.messageTransforms,
)(
    (input: string, object: any, opts?: { field: string }) =>
        input && object[opts.field] !== input,
);

/**
 * Constraints a string to require a proper email format
 */
export const email = createValidation(
    defaults.messages.email,
    defaults.messageTransforms,
)(
    (input: string) =>
        input &&
        !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
            input,
        ),
);

/**
 * Contraints an input to be a string
 */
export const isNumber = createValidation(
    defaults.messages.isNumber,
    defaults.messageTransforms,
)((input: any) => input && isNaN(input));
