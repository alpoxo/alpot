import * as React from "react";
import { SFC } from "react";
import {
    Input,
    FormFieldProps,
    Popup,
    Icon,
    List,
    Header,
    Form,
} from "semantic-ui-react";
import { capitalize } from "lodash";

let PopupList: React.SFC<{ listing: string[]; label: string }> = ({
    listing,
    label,
}) =>
    <div>
        <Header as="h3">
            The {label}
        </Header>
        <List>
            {listing.map(element =>
                <List.Item key={element}>
                    <Icon name="caret right" />
                    <List.Content>
                        {element}
                    </List.Content>
                </List.Item>,
            )}
        </List>
    </div>;

export const Checkbox: SFC<FormFieldProps> = ({
    input,
    label,
    meta: _,
    ...props,
}) => {
    const { value, ...restInput } = input;
    return (
        <Form.Checkbox
            {...restInput}
            checked={!!value}
            onChange={(_, { checked }) => input.onChange(checked)}
            label={capitalize(label)}
            {...props}
        />
    );
};

export const SemanticFormField: SFC<FormFieldProps> = ({
    input,
    meta: { touched, error, warning },
    as: As = Input,
    label,
    icon,
    ...props,
}) => {
    return (
        <As
            {...input}
            value={input.value}
            {...props}
            label={capitalize(label)}
            onChange={input.onChange}
            error={touched && !!error}
            icon={
                (touched &&
                    (error || warning) &&
                    <Popup
                        trigger={
                            <Icon
                                name={error ? "dont" : "warning sign"}
                                color={error ? "red" : "yellow"}
                                size="large"
                                corner
                                fitted
                                link
                            />
                        }
                        content={
                            touched &&
                            ((error &&
                                <PopupList listing={error} label={label} />) ||
                                (warning &&
                                    <PopupList
                                        listing={warning}
                                        label={label}
                                    />))
                        }
                        position="bottom right"
                        on="click"
                        hideOnScroll
                    />) ||
                icon
            }
        />
    );
};
