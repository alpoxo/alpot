/**
 * Lifts a reducer onto an object key.
 *
 * @example
 * 
 *  const registrationRequestReducer = liftReducers({
 *      registrationRequest: promiseReducer(
 *          AuthActionType.Register,
 *          path(["errors", "_error"]),
 *      ),
 *  });
 *  
 *  export const authReducer = reduceReducers(
 *      registrationReducer,
 *      registrationRequestReducer,
 *  );
 * 
 *  In this case the state handled by the promiseReducer
 *  is put onto a key named registrationRequest in the
 *  parent reducer.
 *  
 * @param reducerMap A map of keys with reducers
 */
export const liftReducers = (reducerMap: { [key: string]: any }) => {
    return (previous: any, current: any) =>
        Object.keys(reducerMap).reduce(
            (p, c) => ({
                ...p,
                [c]: reducerMap[c](p[c], current),
            }),
            previous,
        );
};
