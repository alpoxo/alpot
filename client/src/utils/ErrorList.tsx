import * as React from "react";
import * as ReactDOM from "react-dom";
import { TranslationFunction, translate } from "react-i18next";
import { List, Icon } from "semantic-ui-react";
import { capitalize } from "lodash";
import { values } from "lodash";

type ErrorListProps = {
    t: TranslationFunction;
    error: string | { [key: string]: string };
};
const ErrorList = ({ t, error }: ErrorListProps) => {
    if (typeof error === "string")
        return (
            <span>
                {capitalize(t(`error.${error}`))}
            </span>
        );
    return (
        <List>
            {" "}{values(error).map((err: string, i) =>
                <List.Item key={i}>
                    <Icon name="caret right" />
                    <List.Content>
                        {capitalize(t(`error.${err}`))}
                    </List.Content>
                </List.Item>,
            )}
        </List>
    );
};

export default translate()(ErrorList);
