import { handleActions, createAction } from "redux-actions";
import { identity } from "lodash";
import { SubmissionError } from "redux-form";
import { call, put } from "redux-saga/effects";
import { Dispatch } from "redux";

export enum PromiseStatus {
    Initialized = "INITIALIZED",
    Pending = "PENDING",
    Error = "REJECTED",
    Fulfilled = "FULFILLED",
}

export type PromiseError = string | string[] | { [key: string]: string };

export interface PromiseActionState<DataType> {
    status: PromiseStatus;
    error?: PromiseError;
    data?: DataType;
}

export const promiseAction = (prefix: string, status: PromiseStatus): string =>
    prefix + "_" + status;

export const initialPromiseState: PromiseActionState<any> = {
    status: PromiseStatus.Initialized,
};

const defaultErrorSelector = (payload: any) => payload.response.data.error;

export const promiseReducer = (
    prefix: string,
    errorSelector: (payload: any) => PromiseError = defaultErrorSelector,
) =>
    handleActions(
        {
            [promiseAction(prefix, PromiseStatus.Pending)]: state => ({
                ...state,
                status: PromiseStatus.Pending,
                error: null,
                data: null,
            }),
            [promiseAction(prefix, PromiseStatus.Error)]: (state, action) => ({
                ...state,
                status: PromiseStatus.Error,
                error: errorSelector(action.payload),
                data: null,
            }),
            [promiseAction(prefix, PromiseStatus.Fulfilled)]: (
                state,
                action,
            ) => ({
                ...state,
                status: PromiseStatus.Fulfilled,
                error: null,
                data: action.payload,
            }),
        },
        initialPromiseState,
    );

export const isFulfilled = (status: PromiseStatus) =>
    status === PromiseStatus.Fulfilled;

export const isPending = (status: PromiseStatus) =>
    status === PromiseStatus.Pending;

/**
 * Action creators
 */
export const pending = (prefix: string) => ({
    type: promiseAction(prefix, PromiseStatus.Pending),
});

export const fulfilled = (prefix: string, payload: any) => ({
    type: promiseAction(prefix, PromiseStatus.Fulfilled),
    payload,
});

export const error = (prefix: string, error: any) => ({
    type: promiseAction(prefix, PromiseStatus.Error),
    payload: error,
    error: true,
});

/**
 * Promise redux-form helpers for redux-saga
 */
export function metaCreator() {
    const [resolve, reject] = [...arguments].slice(-2);
    return { resolve, reject };
}
export const createPromiseAction = (
    actionType: string,
    payloadCreator: (...args: any[]) => any = identity,
) => {
    const action = (...args: any[]) => (
        dispatch: Dispatch<any>,
    ): Promise<any> =>
        new Promise((resolve, reject) => {
            dispatch(
                createAction(actionType, payloadCreator, metaCreator)(
                    ...args,
                    resolve,
                    reject,
                ),
            );
        });

    (action as any)[Symbol.toPrimitive] = () => actionType;
    (action as any).toString = () => actionType;
    return action;
};

export function formSaga(originalSaga: (...args: any[]) => any) {
    function* newSaga({
        meta: { resolve, reject },
        ...action,
    }: {
        meta: {
            resolve: () => any;
            reject: (err: any) => any;
        };
    }) {
        try {
            const result = yield call(originalSaga, action);
            yield call(resolve, result);
        } catch (error) {
            yield call(reject, error);
        }
    }

    return newSaga;
}

export function standardPromiseSaga(
    prefix: string,
    originalSaga: (...args: any[]) => any,
) {
    function* standardSaga(action: any) {
        try {
            yield put(pending(prefix));
            const response = yield call(originalSaga, action);
            yield put(fulfilled(prefix, response));

            return response;
        } catch (err) {
            yield put(error(prefix, err));

            if (
                err &&
                err.response &&
                err.response.data &&
                err.response.data.error
            )
                throw new SubmissionError({
                    _error:
                        err.response.data.error.fields ||
                        err.response.data.error,
                });

            if (err && err.response && err.response.status === 401)
                throw new SubmissionError({
                    _error: "server-unauthorized",
                });

            if (err && err.message && typeof err.message === "string")
                throw new SubmissionError({
                    _error: err.message,
                });

            // Rethrow error if its not a SubmissionError
            throw err;
        }
    }
    return formSaga(standardSaga);
}
