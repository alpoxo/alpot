import axios from "axios";
import { API } from "../../constants";

export function initializeApi() {
    axios.defaults.baseURL = API;
    axios.defaults.responseType = "json";

    const jwt = JSON.parse(localStorage.getItem("jwt"));
    if (jwt && jwt.token)
        axios.defaults.headers.common["Authorization"] = "Bearer " + jwt.token;
}
