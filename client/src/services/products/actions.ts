import { call, take, select } from "redux-saga/effects";
import axios from "axios";
import { createRequest } from "../registration";
import { requestSuccess } from "..";
import { AuthRequestType } from "../authentication/actions";

export enum ProductActionType {
    CreateProduct = "CREATE_PRODUCT",
    EditProduct = "EDIT_PRODUCT",
    DeleteProduct = "DELETE_PRODUCT",
    GetProducts = "GET_PRODUCTS",
    GetUserProducts = "GET_USER_PRODUCTS",
}

export const createProduct = createRequest(
    ProductActionType.CreateProduct,
    function* createProductSaga({ payload: values }: any) {
        return yield call(axios.post, "/products", values);
    },
);

export const editProduct = createRequest(
    ProductActionType.EditProduct,
    function* editProductSaga({ payload: values }: any) {
        return yield call(axios.patch, "/products/" + values.id, values);
    },
);

export const getProducts = createRequest(
    ProductActionType.GetProducts,
    function* getProducts() {
        return yield call(axios.get, "/products");
    },
);

export const getUserProducts = createRequest(
    ProductActionType.GetUserProducts,
    function* getUserProducts() {
        let user = yield select((state: any) => state.user);
        // If no user is given, wait for the user to log in
        if (!user) {
            const { payload: { data: userFromRequest } } = yield take(
                requestSuccess(AuthRequestType.GetLoggedInUser),
            );
            user = userFromRequest;
        }

        return yield call(axios.get, "/products/user/" + user.username);
    },
);

export const deleteProduct = createRequest(
    ProductActionType.DeleteProduct,
    function* deleteProduct({ payload: product }) {
        return yield call(axios.delete, "/products/" + product.id);
    },
);
