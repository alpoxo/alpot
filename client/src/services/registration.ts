import { createPromiseAction } from "../utils/promise_helper";

export type requestRegistration = {
    [type: string]: (...args: any[]) => any;
};
export const requestRegistration: requestRegistration = {};

/**
 * Creates a request action
 * 
 * @export
 * @param {string} requestType The type of the action
 * @param {(...args: any[]) => any} action The action which should
 *  be executed. Can be a saga (generator) or a promise.
 * @returns The new action
 */
export function createRequest(
    requestType: string,
    action: (...args: any[]) => any,
) {
    const fullType = "@@request/" + requestType;
    const request = createPromiseAction(fullType);

    // register request in sagas
    if (!requestRegistration[fullType]) requestRegistration[fullType] = action;

    return request;
}
