import { call } from "redux-saga/effects";
import axios from "axios";
import { createRequest } from "../registration";

export enum UserActionType {
    GetUser = "GET_USER",
    UpdateUser = "UPDATE_USER",
}

export const getUser = createRequest(
    UserActionType.GetUser,
    function* getUserSaga({ payload: name }: any) {
        yield call(axios.get, "/users/" + name);
    },
);

export const updateUser = createRequest(
    UserActionType.UpdateUser,
    function* updateUserSaga({ payload: user }: any) {
        yield call(axios.patch, "/users/" + user.id, user);
    },
);
