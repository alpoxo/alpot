export function isUserLoggedIn(state: any) {
    return { isUserLoggedIn: !!state.auth };
}
