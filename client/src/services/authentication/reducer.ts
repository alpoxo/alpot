import { handleActions } from "redux-actions";
import { AuthRequestType } from "./actions";
import { requestSuccess } from "..";

export const userReducer = handleActions(
    {
        [requestSuccess(AuthRequestType.GetLoggedInUser)]: (
            _,
            { payload: { data: user } = { data: null } },
        ) => user,
    },
    null,
);

export const authReducer = handleActions(
    {
        [AuthRequestType.SetToken]: (_, { payload: token }) => token,
        [AuthRequestType.RemoveToken]: () => null,
    },
    JSON.parse(localStorage.getItem("jwt")),
);
