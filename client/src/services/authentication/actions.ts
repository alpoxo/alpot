import { call, put } from "redux-saga/effects";
import axios from "axios";
import { createAction } from "redux-actions";
import { createRequest } from "../registration";
import { HASH_SALT, HASH_LENGTH, HASH_ITERATIONS } from "../../constants";
import { initializeApi } from "../api";
import * as pbkdf2 from "pbkdf2";

export enum AuthRequestType {
    Register = "AUTH_REGISTER",
    Login = "AUTH_LOGIN",
    VerifyMail = "AUTH_VERIFY_MAIL",
    GetLoggedInUser = "GET_LOGGED_IN_USER",
    SetToken = "SET_TOKEN",
    RemoveToken = "REMOVE_TOKEN",
}

interface TokenPayload {
    token: String;
    expiration_date: Date;
}

const setToken = createAction<TokenPayload>(AuthRequestType.SetToken);
const removeToken = createAction(AuthRequestType.RemoveToken);

/**
 * Encrypt the password for sending it to the
 * server
 * 
 * @param password The password to encrypt
 */
const encryptPassword = (password: string): Promise<string> =>
    new Promise((resolve, reject) =>
        pbkdf2.pbkdf2(
            password,
            HASH_SALT,
            HASH_ITERATIONS,
            HASH_LENGTH,
            "sha512",
            (err, key) => {
                if (err) reject(err);
                resolve(key.toString("hex"));
            },
        ),
    );

export const register = createRequest(
    AuthRequestType.Register,
    function* registerRequest({ payload: fields }: any) {
        // Encrypt the password before it goes on the line
        fields.password = yield call(encryptPassword, fields.password);

        // Delete not needed repeat field
        delete fields.passwordRepeat;

        return yield call(axios.post, "/auth/register", fields);
    },
);

export const login = createRequest(
    AuthRequestType.Login,
    function* loginRequest({ payload: fields }: any) {
        fields.password = yield call(encryptPassword, fields.password);

        const { data: token } = yield call(axios.post, "/auth/login", fields);

        // Set the token to the local storage for persistence
        localStorage.setItem("jwt", JSON.stringify(token));

        // Also set the token to the state
        yield put(setToken(token));

        initializeApi();
        return token;
    },
);

export const verifyMail = createRequest(
    AuthRequestType.VerifyMail,
    function* verifyMail({ payload: verificationKey }) {
        return yield call(axios.get, `/email-verification/${verificationKey}`);
    },
);

export const getLoggedInUser = createRequest(
    AuthRequestType.GetLoggedInUser,
    function* getLoggedInUserSaga() {
        try {
            return yield call(axios.get, "/auth/current");
        } catch (e) {
            // The user doesn't seem to be logged in!
            localStorage.removeItem("jwt");
            yield put(removeToken());
        }
    },
);
