import { takeEvery } from "redux-saga";
import { combineReducers } from "redux";
import { requestRegistration } from "./registration";
import { standardPromiseSaga, promiseReducer } from "../utils/promise_helper";
import { get } from "lodash/fp";
import "./users/actions";
import "./authentication/actions";
import "./products/actions";

export const requestSuccess = (type: string) =>
    "@@request/" + type + "_FULFILLED";

export const requestError = (type: string) => "@@request/" + type + "_REJECTED";

/**
 * The main service saga. It waits for all the requests
 * 
 * @export
 */
export function* requestSaga() {
    for (const type in requestRegistration) {
        yield takeEvery(
            <any>type,
            standardPromiseSaga(type, requestRegistration[type]),
        );
    }
}

/**
 * Creates a map of reducers
 * from the requests object
 * 
 * @returns The final reducer map
 */
function getReducerMap() {
    const reducerMap: any = {};
    for (const type in requestRegistration) {
        // GET_USER => getUser
        const identifier = type
            .toLowerCase()
            .replace(/[-_]([a-z])/gi, (_, m) => m.toUpperCase())
            .replace(/(^.*\/)/gi, "");

        reducerMap[identifier] = promiseReducer(
            type,
            get(["errors", "_error"]),
        );
    }
    return reducerMap;
}

export const requestReducer = combineReducers(getReducerMap());
