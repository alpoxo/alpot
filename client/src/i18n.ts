import * as i18n from "i18next";
import * as XHR from "i18next-xhr-backend";
import { BackendOptions } from "i18next-xhr-backend";
import * as LanguageDetector from "i18next-browser-languagedetector";

enum InterpolationFormat {
    Uppercase = "uppercase",
}

export default i18n.use(XHR).use(LanguageDetector).init({
    fallbackLng: "en",
    debug: true,
    ns: "common",
    defaultNS: "common",

    interpolation: {
        escapeValue: false, // not needed for react!!
        format: interpolationFormat,
    },

    backend: {
        // for all available options read the backend's repository readme file
        loadPath: "{{lng}}/{{ns}}",
        ajax: loadLocales,
        parse: (data: any) => data,
    },

    // react i18next special options (optional)
    react: {
        wait: false, // set to true if you like to wait for loaded in every translated hoc
        nsMode: "default", // set it to fallback to let passed namespaces to translated hoc act as fallbacks
    },
});

function interpolationFormat(
    value: string,
    format: InterpolationFormat,
    lng: string,
) {
    if (!value) return;
    return {
        [InterpolationFormat.Uppercase]: () => value.toUpperCase(),
    }[format]();
}

function loadLocales(url: string, options: BackendOptions, callback: Function) {
    try {
        require(`bundle-loader!./locales/${url}.json`)((locale: string) =>
            callback(locale, { status: "200" }),
        );
    } catch (e) {
        callback(null, { status: "404" });
    }
}
