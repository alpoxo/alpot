import "ramda";

declare module "ramda" {
    interface Static {
        evolve<T, X extends T & object>(
            transformations: Transformer<X>,
            obj: T,
        ): T;
        evolve<T, X extends T & object>(
            transformations: Transformer<X>,
        ): (obj: T) => T;
    }

    type Transformer<T> = {
        [K in keyof T]?: ((_: T[K]) => T[K]) | Transformer<T[K]>
    };
}

type Diff<T extends string, U extends string> = ({ [P in T]: P } &
    { [P in U]: never } & { [x: string]: never })[T];

type Omit<T, K extends keyof T> = { [P in Diff<keyof T, K>]: T[P] };
