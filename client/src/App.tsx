import * as React from "react";
import { connect } from "react-redux";
import { compose, lifecycle, ComponentEnhancer } from "recompose";
import Home from "./pages/home/Home";
import Profile from "./pages/profile/Profile";
import Shop from "./pages/shop/Shop";
import { MailVerification } from "./pages/authentication/MailVerification";
import {
    HashRouter as Router,
    Route,
    Switch,
    Redirect,
    NavLink,
    RouteProps,
} from "react-router-dom";
import { Menu, Icon, Container } from "semantic-ui-react";
import { getLoggedInUser } from "./services/authentication/actions";
import { isUserLoggedIn } from "./services/authentication";
import styled from "styled-components";

interface AppProps {
    isUserLoggedIn: boolean;
}

const enhance: ComponentEnhancer<AppProps, {}> = compose(
    connect(isUserLoggedIn, dispatch => ({
        getLoggedInUser() {
            return dispatch(getLoggedInUser());
        },
    })),
    lifecycle<AppProps, {}>({
        componentDidMount() {
            if (this.props.isUserLoggedIn)
                (this.props as any).getLoggedInUser();
        },
    }),
);

const ProtectedRoute = connect<AppProps, {}, RouteProps>(
    isUserLoggedIn,
)(({ isUserLoggedIn, component: Component, ...rest }: any) =>
    <Route
        {...rest}
        render={props =>
            !isUserLoggedIn ? <Redirect to="/" /> : <Component {...props} />}
    />,
);

const LeftPanel = styled.div`width: 85px;`;
const MainContainer = styled(Container)`&& { display: flex; }`;
const ContentContainer = styled(Container)`&& { transform: translateY(16px); }`;

const Navigation = ({ isUserLoggedIn }: AppProps) =>
    isUserLoggedIn &&
    <LeftPanel>
        <Menu icon="labeled" fixed="left" vertical>
            <Menu.Item exact as={NavLink} to="/" name="home">
                <Icon name="home" />
                Home
            </Menu.Item>
            <Menu.Item as={NavLink} to="/profile/general" name="profile">
                <Icon name="user circle outline" />
                Profile
            </Menu.Item>
            <Menu.Item as={NavLink} to="/shop" name="shop">
                <Icon name="shop" />
                Shop
            </Menu.Item>
        </Menu>
    </LeftPanel>;

export default enhance(({ isUserLoggedIn }) =>
    <Router basename="/">
        <MainContainer fluid>
            <Navigation isUserLoggedIn={isUserLoggedIn} />
            <ContentContainer fluid>
                <Switch>
                    <Route
                        path="/email-verification/:verificationKey"
                        component={MailVerification}
                    />
                    <ProtectedRoute path="/profile" component={Profile} />
                    <ProtectedRoute path="/shop" component={Shop} />
                    <Route path="/" component={Home} />
                </Switch>
            </ContentContainer>
        </MainContainer>
    </Router>,
);
