import * as React from "react";
import { SemanticFormField } from "../utils/SemanticWrapper";
import { Form, Search, Label } from "semantic-ui-react";
import { capitalize, omit } from "lodash";
import { translate, InjectedTranslateProps } from "react-i18next";
import { getPlatform } from "../here-maps";
import { Address } from "../models/address";

interface GeoServiceFieldProps extends InjectedTranslateProps {
    fieldName: string;
    categoryFieldName: string;
    context: Address;
    maxresults: number;
    onSearchChange: (value: string) => void;
    onResultsReceived: (value: Address[]) => void;
    onResultSelect: (value: Address) => void;
}

type Result = { id: string; title: string };

type ResultCategory = { name: string; results: Result[] };

interface GeoServiceFieldState {
    loading: boolean;
    results: Record<string, ResultCategory>;
    addresses: Address[];
}

const GeoServiceCategorySlot = ({ name }: ResultCategory) =>
    <Label as={"span"} content={name} />;

const GeoServiceCategoryResultSlot = ({ title }: Result) =>
    <h4>
        {title}
    </h4>;

class GeoServiceField extends React.Component<
    GeoServiceFieldProps,
    GeoServiceFieldState
> {
    constructor(props: GeoServiceFieldProps) {
        super(props);
        this.state = {
            loading: false,
            results: null,
            addresses: null,
        };
    }

    mapAddressToServiceRepresentation = (address: Address) => {
        const mappedAddress = { ...address };
        mappedAddress.postalCode = address.plz;
        mappedAddress.county = address.state;

        delete mappedAddress.plz;
        delete mappedAddress.state;

        return mappedAddress;
    };

    mapServiceToAddressRepresentation = (obj: any) => {
        const address: Address = { ...obj };
        address.plz = obj.postalCode;
        address.state = obj.county;

        delete address.postalCode;
        delete address.county;

        return address;
    };

    mapAddressesToCategories = (addresses: Address[]) => {
        const categories: Record<string, ResultCategory> = {};

        for (var address of addresses) {
            let category = categories[address[this.props.categoryFieldName]];
            if (!category)
                categories[address[this.props.categoryFieldName]] = category = {
                    name: address[this.props.categoryFieldName].toString(),
                    results: [],
                };

            const id =
                address[this.props.categoryFieldName] +
                ":" +
                address[this.props.fieldName] +
                ":" +
                address.plz;

            const result = {
                id,
                childKey: id,
                title: address[this.props.fieldName].toString(),
            };

            category.results.push(result);
        }

        return categories;
    };

    handleServiceSuccess = (result: H.service.ServiceResult) => {
        // As any because the property view seems to be missing
        const view =
            result && result.response && (result.response as any).view[0];

        // Stop loading state
        this.setState({ loading: false });

        // We found no results!
        if (!view) return this.setState({ results: null });

        // Prepare received addresses for usage
        // This includes mapping of the addresses to
        // our internal representation
        const receivedAddresses: Address[] = view.result
            .map(({ location }: any) => ({
                ...this.mapServiceToAddressRepresentation(location.address),
                lat: location.displayPosition.latitude,
                lng: location.displayPosition.longitude,
            }))
            .filter((address: Address) => address[this.props.fieldName])
            .slice(0, 6);

        // Call event handler of parent
        if (this.props.onResultsReceived)
            this.props.onResultsReceived(receivedAddresses);

        const addressCategories = this.mapAddressesToCategories(
            receivedAddresses,
        );

        this.setState({
            addresses: receivedAddresses,
            results: addressCategories,
        });
    };

    handleServiceError = () => this.setState({ loading: false });

    handleSearchChange = (value: string) => {
        const contextCopy = {
            ...this.props.context,
            [this.props.fieldName]: value,
        };
        const addressParams = this.mapAddressToServiceRepresentation(
            contextCopy,
        );

        if (this.props.onSearchChange) this.props.onSearchChange(value);

        const service = getPlatform().getGeocodingService();
        const params: H.service.ServiceParameters = {
            ...addressParams as any,
            maxresults: (this.props.maxresults || 4).toString(),
            jsonattributes: "1",
        };

        // We are loading some new data now
        this.setState({ loading: true });

        service.geocode(
            params,
            this.handleServiceSuccess,
            this.handleServiceError,
        );
    };

    handleResultSelect = (value: Result) => {
        const [categoryFieldValue, fieldValue, plz] = value.id.split(":");
        const address = this.state.addresses.find(
            address =>
                address[this.props.categoryFieldName] === categoryFieldValue &&
                address[this.props.fieldName] === fieldValue &&
                address.plz === plz,
        );

        if (this.props.onResultSelect) this.props.onResultSelect(address);
    };

    render() {
        const { t, fieldName, onResultSelect, ...restProps } = this.props;
        return (
            <SemanticFormField
                category
                selectFirstResult
                icon="globe"
                as={Form.Field}
                control={Search}
                label={capitalize(this.props.t(this.props.fieldName))}
                categoryRenderer={GeoServiceCategorySlot}
                resultRenderer={GeoServiceCategoryResultSlot}
                loading={this.state.loading}
                results={this.state.results}
                onResultSelect={(_: any, value: Result) =>
                    this.handleResultSelect(value)}
                onSearchChange={(_: any, value: string) =>
                    this.handleSearchChange(value)}
                {...omit(restProps, [
                    "i18n",
                    "i18nLoadedAt",
                    "onSearchChange",
                    "context",
                    "maxresults",
                    "onResultsReceived",
                    "categoryFieldName",
                ])}
            />
        );
    }
}

export default translate()(GeoServiceField);
