// ---------------------------------------------------------------------------
// **** Setup React with Redux ****
// ---------------------------------------------------------------------------

import * as React from "react";
import * as ReactDOM from "react-dom";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import "../semantic/dist/semantic.min.css";
import { rootReducer, rootSaga } from "./root";
import { devToolsEnhancer } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";
import thunk from "redux-thunk";
import App from "./App";
import { I18nextProvider } from "react-i18next";
import i18n from "./i18n";
import { initializeApi } from "./services/api";
import { initializePlatform } from "./here-maps";
import { HERE_MAP_APP_ID, HERE_MAP_APP_CODE } from "./constants";

declare var require: (moduleId: string) => any;
declare var module: { hot: any };
import { AppContainer } from "react-hot-loader";

// Initialize the api
initializeApi();
initializePlatform(HERE_MAP_APP_ID, HERE_MAP_APP_CODE);

// Create redux store
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    rootReducer,
    compose(applyMiddleware(sagaMiddleware, thunk), devToolsEnhancer({})),
);

// Start sagas
sagaMiddleware.run(rootSaga);

// Render method for normal and hot-reloader use
const render = (Component: React.ComponentClass) => {
    ReactDOM.render(
        <AppContainer>
            <Provider store={store}>
                <I18nextProvider i18n={i18n}>
                    <Component />
                </I18nextProvider>
            </Provider>
        </AppContainer>,
        document.getElementById("root"),
    );
};

render(App);

// Setup hot reloader
if (module.hot) {
    module.hot.accept("./App", () => {
        const NextApp = require("./App").default;
        render(NextApp);
    });

    module.hot.accept("./root", () => {
        store.replaceReducer(rootReducer);
    });
}
