import { handleActions } from "redux-actions";
import { ProductActionType } from "../../services/products/actions";
import { Product } from "../../models/product";
import { requestSuccess } from "../../services";

interface ShopState {
    products: Product[];
}

const initialState: ShopState = {
    products: [],
};

export const shopReducer = handleActions<ShopState, { data: Product[] }>(
    {
        [requestSuccess(ProductActionType.GetUserProducts)]: (
            state,
            { payload: { data: products } },
        ) => ({
            ...state,
            products: products.sort((a, b) => a.id - b.id),
        }),
    },
    initialState,
);
