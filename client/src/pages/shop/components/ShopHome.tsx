import * as React from "react";
import {
    Button,
    Card,
    Container,
    Icon,
    Segment,
    Divider,
} from "semantic-ui-react";
import { Product } from "../../../models/product";
import { User } from "../../../models/user";
import { ComponentEnhancer, compose } from "recompose";
import { connect } from "react-redux";
import { translate, InjectedTranslateProps } from "react-i18next";
import { capitalize, pick } from "lodash/fp";
import { isPending, PromiseStatus } from "../../../utils/promise_helper";
import { Link } from "react-router-dom";
import styled from "styled-components";

interface ProductCreatorProps extends InjectedTranslateProps {
    user: User;
    products: Product[];
    loadStatus: PromiseStatus;
    getProducts: (user: User) => Promise<Product[]>;
}

const enhance: ComponentEnhancer<ProductCreatorProps, any> = compose(
    connect(
        ({
            requests: { getUserProducts: { status: loadStatus } },
            shop: { products },
        }) => ({
            loadStatus,
            products,
        }),
    ),
    translate(),
);

const Products = ({
    t,
    products,
}: { products: Product[] } & InjectedTranslateProps) =>
    <div>
        <Card.Group>
            {products &&
                products.map(product =>
                    <Card
                        key={product.id}
                        as={Link}
                        to={"/shop/products/edit/" + product.id}
                    >
                        <Card.Content header={product.name} />
                        <Card.Content description={product.description} />
                    </Card>,
                )}
        </Card.Group>
    </div>;

const ShopContainer = styled(Container)`
    padding-top: 16px;
`;

export default enhance(({ t, loadStatus, products }) =>
    <ShopContainer>
        <Segment loading={isPending(loadStatus)}>
            <Button as={Link} to="/shop/products/new">
                {capitalize(t("create")) + " " + t("product")}
            </Button>
            <Divider />
            <Products t={t} products={products} />
        </Segment>
    </ShopContainer>,
);
