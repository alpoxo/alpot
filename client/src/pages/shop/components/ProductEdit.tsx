import * as React from "react";
import { Header, Form, Message, Input, Segment } from "semantic-ui-react";
import { Field, FormProps, reduxForm } from "redux-form";
import { Product } from "../../../models/product";
import { ComponentEnhancer, compose } from "recompose";
import { connect } from "react-redux";
import { translate, InjectedTranslateProps } from "react-i18next";
import { get, capitalize, pick, find } from "lodash/fp";
import { validate, v, isRequired } from "../../../utils/validation";
import { isPending } from "../../../utils/promise_helper";
import { SemanticFormField, Checkbox } from "../../../utils/SemanticWrapper";
import ErrorList from "../../../utils/ErrorList";
import { match, Prompt, RouteComponentProps } from "react-router-dom";
import {
    createProduct,
    editProduct,
    getUserProducts,
} from "../../../services/products/actions";

interface RouteMatchParams {
    id: number;
}

interface ProductEditProps
    extends InjectedTranslateProps,
        RouteComponentProps<any>,
        FormProps<Product, ProductEditProps, {}> {
    products: Product[];
    loading: boolean;
}

const productValidation = validate({
    name: v(isRequired()),
    description: v(isRequired()),
    price: v(isRequired()),
});

const enhance: ComponentEnhancer<ProductEditProps, any> = compose(
    connect(
        ({
            requests: {
                createProduct: { status: createProductStatus },
                editProduct: { status: editProductStatus },
                getUserProducts: { status: getUserProductsStatus },
            },
            shop: { products },
        }) => ({
            loading:
                isPending(createProductStatus) ||
                isPending(editProductStatus) ||
                isPending(getUserProductsStatus),
            products,
        }),
        {},
        ({ products, ...stateProps }, dispatchProps, ownProps: any) => ({
            ...stateProps,
            ...dispatchProps,
            ...ownProps,
            initialValues: find(
                { id: parseInt(ownProps.match.params.id) },
                products,
            ),
        }),
    ),
    reduxForm<Product, ProductEditProps, {}>({
        form: "product_create",
        validate: productValidation,
        enableReinitialize: true,
        onSubmit(values: Product, dispatch, props) {
            const action = isCreation(props.match)
                ? createProduct
                : editProduct;
            return dispatch(action(values));
        },
        async onSubmitSuccess({ data: createdProduct }, dispatch, props) {
            await dispatch(getUserProducts());
            if (isCreation(props.match))
                props.history.push("/shop/products/edit/" + createdProduct.id);
        },
    }),
);

const isCreation = (match: match<RouteMatchParams>) => !match.params.id;

const GeneralForm = enhance(
    ({ dirty, match, handleSubmit, submitSucceeded, t, error, loading }) =>
        <div>
            <Prompt
                when={dirty && !submitSucceeded}
                message={t("prompt-form-redirect")}
            />
            <Form onSubmit={handleSubmit} loading={loading} error={!!error}>
                <Field
                    component={SemanticFormField}
                    as={Form.Field}
                    control={Input}
                    label={capitalize(t("name"))}
                    name="name"
                    type="text"
                />
                <Field
                    component={SemanticFormField}
                    as={Form.Field}
                    control={Input}
                    label={capitalize(t("description"))}
                    name="description"
                    type="text"
                />
                <Field
                    component={SemanticFormField}
                    as={Form.Field}
                    control={Input}
                    label={capitalize(t("price"))}
                    name="price"
                    type="number"
                />
                <Field
                    component={SemanticFormField}
                    as={Form.Field}
                    control={Input}
                    label={capitalize(t("amount"))}
                    name="amount"
                    type="number"
                />
                <Field
                    component={SemanticFormField}
                    as={Form.Field}
                    control={Input}
                    label={capitalize(t("origin"))}
                    name="origin"
                    type="text"
                />
                <Field
                    component={Checkbox}
                    label={capitalize(t("food"))}
                    name="is_food"
                    normalize={(value: boolean) => !!value}
                />
                {error &&
                    <Message
                        error
                        header={t("form-errors")}
                        content={<ErrorList t={t} error={error} />}
                    />}
                <Form.Button>
                    {capitalize(t(isCreation(match) ? "create" : "edit"))}
                </Form.Button>
            </Form>
        </div>,
);

export default translate()(
    ({
        t,
        match,
        history,
    }: InjectedTranslateProps & RouteComponentProps<any>) =>
        <Segment>
            <Header>
                {capitalize(t(isCreation(match) ? "create" : "edit")) +
                    " " +
                    t("product")}
            </Header>
            <GeneralForm t={t} match={match} history={history} />
        </Segment>,
);
