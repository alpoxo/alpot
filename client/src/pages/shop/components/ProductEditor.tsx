import * as React from "react";
import { compose, withState, withHandlers, ComponentEnhancer } from "recompose";
import { Product } from "../../../models/product";
import { isPending } from "../../../utils/promise_helper";
import {
    deleteProduct,
    getUserProducts,
} from "../../../services/products/actions";
import { connect } from "react-redux";
import ProductEdit from "./ProductEdit";
import {
    Route,
    Switch,
    NavLink,
    Link,
    RouteComponentProps,
} from "react-router-dom";
import {
    Container,
    Confirm,
    Grid,
    Menu,
    Label,
    Button,
    Icon,
    Segment,
} from "semantic-ui-react";
import styled from "styled-components";
import { get, capitalize } from "lodash/fp";
import { translate, InjectedTranslateProps } from "react-i18next";

// ########################################################################
// Sidebar for the list of items on the left
// ########################################################################
const NewProductButton = styled(Button)`
&& {
    position: relative;
    left: 50%;
    transform: translateX(-50%);

    &&&.active {
        pointer-events: none;
        color: grey !important;
        box-shadow: 0 0 0 1px grey inset!important;
    }
}`;

const DeleteButton = styled(Button)`
&&&&& {
    box-shadow: 0 0 0 2px #ff5144 inset!important;
    color: #ff695e!important;
    background-color: transparent!important;
    opacity: 0.5;

    & > i {
        opacity: 1;
    }

    &:hover {
        opacity: 1;
    }
}`;

const BorderRadiusLessMenu = styled(Menu)`
    border-radius: 0!important;
`;

const BottomMenu = styled(BorderRadiusLessMenu)`
&&& {
    position: absolute;
    bottom: 0;
    width: 100%;
}`;

const MenuSegment = styled(Segment)`
&& {
    padding: 0;
}`;

const LeaveLink = styled(Menu.Item)`
    text-align: center;

    &&&&& > i {
        float: none;
        margin: 0 0.5em 0 0;
    }
`;

const ItemList = ({
    t,
    products,
    selectedProduct,
    setSelectedProduct,
    deleteConfirmationOpen,
    openDeleteConfirmation,
    cancelDeleteConfirmation,
    confirmDeleteConfirmation,
}: ProductEditorProps) =>
    <MenuSegment inverted>
        <BorderRadiusLessMenu inverted vertical pointing borderless fluid>
            {selectedProduct &&
                <Confirm
                    open={deleteConfirmationOpen}
                    header={capitalize(t("confirmation"))}
                    content={capitalize(
                        t("confirmation-text", {
                            what: t("delete") + ` ${selectedProduct.name}`,
                        }),
                    )}
                    onCancel={() => cancelDeleteConfirmation()}
                    onConfirm={() => confirmDeleteConfirmation(selectedProduct)}
                />}
            {products.map(product =>
                <Menu.Item
                    as={NavLink}
                    to={"/shop/products/edit/" + product.id}
                    key={product.id}
                    name={product.name}
                    onClick={() => setSelectedProduct(product)}
                >
                    <DeleteButton
                        as={Label}
                        onClick={e => {
                            e.preventDefault();
                            openDeleteConfirmation(product);
                        }}
                        icon="delete"
                        color="red"
                        inverted
                        basic
                    />
                    {product.name}
                </Menu.Item>,
            )}
            <Menu.Item>
                <NewProductButton
                    as={NavLink}
                    to="/shop/products/new"
                    icon="plus"
                    color="green"
                    circular
                    basic
                />
            </Menu.Item>
        </BorderRadiusLessMenu>
        <BottomMenu size="huge" inverted vertical fluid>
            <LeaveLink as={Link} to="/shop">
                <Icon name="sign out" flipped="horizontally" />
                {capitalize(t("leave"))}
            </LeaveLink>
        </BottomMenu>
    </MenuSegment>;

// ########################################################################
// Editor Main Layout
// ########################################################################
interface ProductEditorProps
    extends InjectedTranslateProps,
        RouteComponentProps<{ id: number }> {
    products: Product[];
    deleteProduct: (product: Product) => Promise<Product[]>;
    selectedProduct: Product;
    setDeleteConfirmation: (confirmation: boolean) => void;
    setSelectedProduct: (product: Product) => void;
    deleteConfirmationOpen: boolean;
    openDeleteConfirmation: (product: Product) => void;
    cancelDeleteConfirmation: () => void;
    confirmDeleteConfirmation: (product: Product) => void;
}

const enhance: ComponentEnhancer<
    ProductEditorProps,
    RouteComponentProps<any>
> = compose(
    connect(
        ({
            shop: { products },
            requests: { getUserProducts: { status } },
        }) => ({ products, loading: isPending(status) }),
        dispatch => ({
            async deleteProduct(product): Promise<Product[]> {
                await dispatch(deleteProduct(product));
                const { data: newProducts } = await dispatch(getUserProducts());
                return newProducts;
            },
        }),
    ),
    withState("selectedProduct", "setSelectedProduct", null),
    withState("deleteConfirmationOpen", "setDeleteConfirmation", false),
    withHandlers({
        openDeleteConfirmation: ({
            setDeleteConfirmation,
            setSelectedProduct,
        }) => (product: Product) => {
            setSelectedProduct(product);
            setDeleteConfirmation(true);
        },
        cancelDeleteConfirmation: ({ setDeleteConfirmation }) => () =>
            setDeleteConfirmation(false),
        confirmDeleteConfirmation: ({
            setDeleteConfirmation,
            deleteProduct,
            selectedProduct,
            history,
        }: ProductEditorProps) => async (product: Product) => {
            setDeleteConfirmation(false);
            const products = await deleteProduct(product);
            if (selectedProduct.id === product.id && products.length)
                history.push("/shop/products/edit/" + products[0].id);
        },
    }),
    translate(),
);

const RightContent = styled(Container)`
    padding: 16px;
`;
const GridColumn = styled(Grid.Column)`
&&&& {
    padding-top: 0;
    padding-bottom: 0;
}`;

export default enhance(props =>
    <Grid stretched>
        <GridColumn width={3} stretched>
            <ItemList {...props} />
        </GridColumn>
        <GridColumn width={13} stretched>
            <RightContent fluid>
                <Switch>
                    <Route path="/shop/products/new" component={ProductEdit} />
                    <Route
                        path="/shop/products/edit/:id"
                        component={ProductEdit}
                    />
                </Switch>
            </RightContent>
        </GridColumn>
    </Grid>,
);
