import { connect } from "react-redux";
import { compose, ComponentEnhancer, lifecycle } from "recompose";
import { Product } from "../../../models/product";
import { getUserProducts } from "../../../services/products/actions";
import { isEmpty } from "lodash";

interface GetProductsHOCProps {
    products: Product[];
    getProducts: () => Promise<Product[]>;
}

const enhance: ComponentEnhancer<GetProductsHOCProps, any> = compose(
    connect(
        ({ shop: { products } }) => ({ products }),
        dispatch => ({
            getProducts: () => dispatch(getUserProducts()),
        }),
    ),
    lifecycle<GetProductsHOCProps, {}>({
        componentDidMount() {
            if (isEmpty(this.props.products)) this.props.getProducts();
        },
    }),
);

// We render nothing. We just want to load stuff here
export default enhance((): null => null);
