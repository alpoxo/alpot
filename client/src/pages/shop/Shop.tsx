import * as React from "react";
import { Route } from "react-router-dom";
import ProductEditor from "./components/ProductEditor";
import ShopHome from "./components/ShopHome";
import GetProductsHOC from "./components/GetProductsHOC";
import styled from "styled-components";

const Wrapper = styled.div`
    margin-top: -16px;
    height: 100vh;
    & > div {
        min-height: 100%;
    }
`;

export default () =>
    <Wrapper>
        <Route path="/shop/products" component={ProductEditor} />
        <Route exact path="/shop" component={ShopHome} />
        <Route path="/shop" component={GetProductsHOC} />
    </Wrapper>;
