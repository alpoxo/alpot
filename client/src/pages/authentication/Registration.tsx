import * as React from "react";
import { Field, reduxForm, FormProps } from "redux-form";
import { compose, withState, ComponentEnhancer } from "recompose";
import { connect } from "react-redux";
import { SemanticFormField } from "../../utils/SemanticWrapper";
import { RegistrationProperties } from "./types";
import { register } from "../../services/authentication/actions";
import { get } from "lodash/fp";
import { translate, InjectedTranslateProps } from "react-i18next";
import { capitalize } from "lodash";
import { isPending, PromiseStatus } from "../../utils/promise_helper";
import { RouteComponentProps } from "react-router-dom";
import {
    v,
    validate,
    isRequired,
    length,
    passwordComplexity,
    email,
    match,
} from "../../utils/validation";
import { Modal, Button, Form, Icon, Message, Input } from "semantic-ui-react";
import ErrorList from "../../utils/ErrorList";

const validateForm = validate({
    username: v(isRequired()),
    password: v(
        isRequired(),
        length({
            length: 6,
        }),
        passwordComplexity(),
    ),
    passwordRepeat: v(
        isRequired(),
        match({
            field: "password",
            message: "validation.password-match",
        }),
    ),
    email: v(isRequired(), email()),
});

interface RegistrationProps
    extends InjectedTranslateProps,
        FormProps<RegistrationProperties, RegistrationProps, {}> {
    authRegister: { status: PromiseStatus };
    setSuccess: (newSuccess: boolean) => void;
}

const enhance: ComponentEnhancer<
    RegistrationProps,
    {
        setSuccess: (newSuccess: boolean) => void;
    }
> = compose(
    connect(get("requests")),
    reduxForm({
        form: "registration",
        validate: validateForm,
        onSubmit: function(
            values: RegistrationProperties,
            dispatch,
            props: RegistrationProps,
        ) {
            return dispatch(register(values)).then(() => {
                props.setSuccess(true);
            });
        },
    }),
    translate(),
);

const RegistrationForm = enhance(
    ({ error, handleSubmit, authRegister: { status }, t }) =>
        <Form
            onSubmit={handleSubmit}
            loading={isPending(status)}
            error={!!error}
        >
            <Field
                component={SemanticFormField}
                as={Form.Field}
                control={Input}
                label={t("username")}
                name="username"
                type="text"
            />
            <Field
                component={SemanticFormField}
                as={Form.Field}
                control={Input}
                label={t("email")}
                name="email.email"
                type="text"
            />
            <Field
                component={SemanticFormField}
                as={Form.Field}
                control={Input}
                label={t("password")}
                name="password"
                type="password"
            />
            <Field
                component={SemanticFormField}
                as={Form.Field}
                control={Input}
                label={t("password-repeat")}
                name="passwordRepeat"
                type="password"
            />
            {error &&
                <Message
                    error
                    header={t("form-errors")}
                    content={<ErrorList t={t} error={error} />}
                />}
            <Button type="submit" primary>
                <Icon name="unlock" />
                {capitalize(t("register"))}
            </Button>
        </Form>,
);

interface RegistrationModalProps
    extends InjectedTranslateProps,
        RouteComponentProps<any> {
    authRegister: { status: PromiseStatus };
    setSuccess: (newSuccess: boolean) => void;
    success: boolean;
}

const enhanceModal: ComponentEnhancer<
    RegistrationModalProps,
    RouteComponentProps<any>
> = compose(
    connect(get("requests")),
    translate(),
    withState("success", "setSuccess", false),
);

const RegistrationModal = enhanceModal(({ t, history, setSuccess, success }) =>
    <div>
        <Modal open={success} basic={true} size="small">
            <Modal.Header>
                {capitalize(t("registration-success-header"))}
            </Modal.Header>
            <Modal.Content>
                {capitalize(t("registration-success-text"))}
            </Modal.Content>
            <Modal.Actions>
                <Button
                    onClick={e => {
                        setSuccess(false);
                        e.stopPropagation();
                        history.replace("/login");
                    }}
                    color="green"
                    inverted
                >
                    <Icon name="checkmark" /> Ok
                </Button>
            </Modal.Actions>
        </Modal>
        <Modal
            open={!success}
            onClose={e => {
                e.stopPropagation();
                history.replace("/");
            }}
            size="small"
        >
            <Modal.Header>
                {capitalize(t("registration"))}
            </Modal.Header>
            <Modal.Content>
                <RegistrationForm setSuccess={setSuccess} />
            </Modal.Content>
        </Modal>
    </div>,
);

export default RegistrationModal;
