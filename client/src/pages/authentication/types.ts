import { Action } from "redux";
import { PromiseActionState } from "../../utils/promise_helper";

export interface RegistrationProperties {
    username: string;
    email: string;
    password: string;
}

export interface LoginProperties {
    username: string;
    password: string;
}
