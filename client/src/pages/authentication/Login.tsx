import * as React from "react";
import { Modal, Button, Form, Icon, Message, Input } from "semantic-ui-react";
import { Field, reduxForm, FormProps } from "redux-form";
import { connect } from "react-redux";
import { compose, withState, ComponentEnhancer } from "recompose";
import { LoginProperties } from "./types";
import { login, getLoggedInUser } from "../../services/authentication/actions";
import { translate, InjectedTranslateProps } from "react-i18next";
import { get } from "lodash/fp";
import { PromiseStatus, isPending } from "../../utils/promise_helper";
import { SemanticFormField } from "../../utils/SemanticWrapper";
import ErrorList from "../../utils/ErrorList";
import { v, validate, isRequired } from "../../utils/validation";
import { capitalize } from "lodash";
import { Redirect, RouteComponentProps } from "react-router-dom";

interface LoginProps
    extends InjectedTranslateProps,
        FormProps<LoginProperties, LoginProps, {}> {
    login: () => void;
    authLogin: { status: PromiseStatus };
    getLoggedInUser: { status: PromiseStatus };
    setSuccess: (state: boolean) => void;
    success: boolean;
}

const loginValidation = validate({
    username: v(isRequired()),
    password: v(isRequired()),
});

const enhance: ComponentEnhancer<LoginProps, {}> = compose(
    withState("success", "setSuccess", false),
    connect(get("requests")),
    reduxForm({
        form: "login",
        validate: loginValidation,
        onSubmit: (values: LoginProperties, dispatch) =>
            dispatch(login(values)),
        onSubmitSuccess: (_, dispatch, props: LoginProps) =>
            dispatch(getLoggedInUser()).then(() => props.setSuccess(true)),
    }),
    translate(),
);

const LoginForm = enhance(
    ({
        success,
        error,
        handleSubmit,
        authLogin: { status },
        getLoggedInUser: { status: loggedInUserStatus },
        t,
    }) => {
        if (success) return <Redirect to="/profile" />;
        return (
            <Form
                onSubmit={handleSubmit}
                loading={isPending(status) || isPending(loggedInUserStatus)}
                error={!!error}
            >
                <Field
                    component={SemanticFormField}
                    as={Form.Field}
                    control={Input}
                    label={capitalize(t("username"))}
                    name="username"
                    type="text"
                />
                <Field
                    component={SemanticFormField}
                    as={Form.Field}
                    control={Input}
                    label={capitalize(t("password"))}
                    name="password"
                    type="password"
                />
                {error &&
                    <Message
                        error
                        header={t("form-errors")}
                        content={<ErrorList t={t} error={error} />}
                    />}
                <Button type="submit" primary>
                    <Icon name="unlock" />Login
                </Button>
            </Form>
        );
    },
);

const LoginModal = ({ history }: RouteComponentProps<any>) =>
    <Modal
        open={true}
        onClose={e => {
            e.stopPropagation();
            history.replace("/");
        }}
        size="small"
    >
        <Modal.Header>Login</Modal.Header>
        <Modal.Content>
            <LoginForm />
        </Modal.Content>
    </Modal>;

export default LoginModal;
