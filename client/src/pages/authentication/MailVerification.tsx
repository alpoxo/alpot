import * as React from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import { capitalize } from "lodash";
import { translate, InjectedTranslateProps } from "react-i18next";
import { compose, lifecycle, ComponentEnhancer } from "recompose";
import { verifyMail } from "../../services/authentication/actions";
import { get } from "lodash/fp";
import { connect } from "react-redux";
import { PromiseStatus, PromiseActionState } from "../../utils/promise_helper";
import {
    Icon,
    Modal,
    Button,
    Dimmer,
    Header,
    Loader,
    Segment,
    SemanticCOLORS,
} from "semantic-ui-react";

interface RouteParams {
    verificationKey: string;
}

type MailVerificationProperties = InjectedTranslateProps &
    RouteComponentProps<RouteParams> &
    PromiseActionState<any> & { verifyMail: (verificationkey: string) => void };

const enhance: ComponentEnhancer<
    MailVerificationProperties,
    MailVerificationProperties
> = compose(
    connect(get(["requests", "authVerifyMail"]), dispatch => ({
        verifyMail: (verificationKey: string) =>
            dispatch(verifyMail(verificationKey)),
    })),
    lifecycle<MailVerificationProperties, {}>({
        componentDidMount: function() {
            const { verificationKey } = this.props.match.params;
            if (verificationKey) this.props.verifyMail(verificationKey);
        },
    }),
    translate(),
);

export const MailVerificationMessage = ({
    t,
    color,
    text,
    icon,
    shortMessage,
}: InjectedTranslateProps & {
    color: SemanticCOLORS;
    text: string;
    icon: string;
    shortMessage: string;
}) =>
    <div>
        <Header as="h2" icon inverted>
            <Icon name={icon} color={color} />
            {capitalize(t("verification.email-header"))} {shortMessage}
            <Header.Subheader>{capitalize(t(text))}</Header.Subheader>
        </Header>
        <Segment basic>
            <Button
                as={Link}
                to="/"
                color={color}
                content={capitalize(t("home"))}
                inverted
            />
            <Button
                as={Link}
                to="/login"
                color={color}
                content={capitalize(t("login"))}
                inverted
            />
        </Segment>
    </div>;

export const MailVerification = enhance(
    ({ t, verifyMail, error, status, match }) => {
        const isPending = status === PromiseStatus.Pending;
        const isSuccess = status === PromiseStatus.Fulfilled;
        const isError = status === PromiseStatus.Error;
        return (
            <div>
                <Dimmer active page>
                    <Loader
                        size="huge"
                        active={isPending}
                        disabled={!isPending}
                    >
                        {t("verifying") + " " + t("email")}
                    </Loader>
                    {isSuccess &&
                        <MailVerificationMessage
                            color="green"
                            text={capitalize(
                                t("verification.email-success-text"),
                            )}
                            shortMessage={t("success")}
                            icon="checkmark"
                            t={t}
                        />}

                    {isError &&
                        <MailVerificationMessage
                            color="red"
                            text={
                                capitalize(t("verification.email-error-text")) +
                                " " +
                                t(`verification.${error}`)
                            }
                            shortMessage={t("failure")}
                            icon="remove"
                            t={t}
                        />}
                </Dimmer>
            </div>
        );
    },
);
