import * as React from "react";
import Login from "../authentication/Login";
import Registration from "../authentication/Registration";
import { Button, Container } from "semantic-ui-react";
import { Route, Link, RouteComponentProps } from "react-router-dom";
import { capitalize } from "lodash";
import { compose, ComponentEnhancer } from "recompose";
import { translate, InjectedTranslateProps } from "react-i18next";

const enhance: ComponentEnhancer<
    InjectedTranslateProps,
    RouteComponentProps<any>
> = compose(translate());

export default enhance(({ t }) =>
    <Container>
        <Route path="/registration" component={Registration} />
        <Route path="/login" component={Login} />

        <Button as={Link} to="/registration">
            {capitalize(t("registration"))}
        </Button>
        <Button as={Link} to="/login">
            {capitalize(t("login"))}
        </Button>
    </Container>,
);
