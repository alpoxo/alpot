import * as React from "react";
import { Header, Form, Message, Icon, Input } from "semantic-ui-react";
import { Field, FormProps, reduxForm } from "redux-form";
import { User } from "../../../models/user";
import { ComponentEnhancer, compose } from "recompose";
import { connect } from "react-redux";
import { InjectedTranslateProps } from "react-i18next";
import { get, capitalize, pick } from "lodash/fp";
import { validate, v, isRequired, email } from "../../../utils/validation";
import {
    PromiseStatus,
    PromiseActionState,
} from "../../../utils/promise_helper";
import { SemanticFormField, Checkbox } from "../../../utils/SemanticWrapper";
import ErrorList from "../../../utils/ErrorList";
import { updateUser } from "../../../services/users/actions";

interface GeneralPartProps
    extends InjectedTranslateProps,
        FormProps<User, GeneralPartProps, {}> {
    user: User;
    updateUser: PromiseActionState<User>;
}

const userValidation = validate({
    username: v(isRequired()),
    email: v(isRequired(), email()),
});

const enhance: ComponentEnhancer<GeneralPartProps, any> = compose(
    connect(get("requests")),
    reduxForm({
        form: "profile_general",
        validate: userValidation,
        onSubmit: (values: User, dispatch) => dispatch(updateUser(values)),
    }),
);

const GeneralForm = enhance(
    ({ handleSubmit, t, error, updateUser: { status } }) =>
        <Form
            onSubmit={handleSubmit}
            loading={status === PromiseStatus.Pending}
            error={!!error}
        >
            <Field
                component={SemanticFormField}
                as={Form.Field}
                control={Input}
                label={capitalize(t("username"))}
                name="username"
                type="text"
            />
            <Field
                component={SemanticFormField}
                as={Form.Field}
                control={Input}
                label={capitalize(t("email"))}
                name="email.email"
                type="email"
            />
            <Field
                component={Checkbox}
                label={capitalize(t("trader"))}
                name="is_trader"
                normalize={(value: boolean) => !!value}
            />
            {error &&
                <Message
                    error
                    header={t("form-errors")}
                    content={<ErrorList t={t} error={error} />}
                />}
            <Form.Button>
                <Icon name="edit" />Update
            </Form.Button>
        </Form>,
);

export default ({ user, t }: InjectedTranslateProps & { user: User }) =>
    <div>
        <Header>
            {t("profile.general-header")}
        </Header>
        <GeneralForm initialValues={user} t={t} />
    </div>;
