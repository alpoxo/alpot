import * as React from "react";
import { InjectedTranslateProps } from "react-i18next";
import { Header, Form, Message, Search, Icon, Label } from "semantic-ui-react";
import {
    ComponentEnhancer,
    compose,
    withState,
    withHandlers,
    withProps,
    lifecycle,
} from "recompose";
import { connect } from "react-redux";
import { reduxForm, FormProps, Field, getFormValues } from "redux-form";
import { merge } from "lodash";
import { get, capitalize } from "lodash/fp";
import { isPending } from "../../../utils/promise_helper";
import { updateUser } from "../../../services/users/actions";
import { getLoggedInUser } from "../../../services/authentication/actions";
import { User } from "../../../models/user";
import { validate, v, isRequired, isNumber } from "../../../utils/validation";
import ErrorList from "../../../utils/ErrorList";
import { HereMap, MapEvent, Marker } from "../../../here-maps";
import { getPlatform } from "../../../here-maps";
import GeoServiceField from "../../../components/GeoServiceField";
import { Address } from "../../../models/address";

interface Location {
    street: string;
    plz: string;
    city: string;
    country: string;
    state: string;
    houseNumber: number;
    lat: number;
    lng: number;
}

interface ProfileInformationProps
    extends InjectedTranslateProps,
        FormProps<User, ProfileInformationProps, {}> {
    user: User;
    loading: boolean;
    loadingLocation: boolean;
    loadingFields: Record<
        string,
        { loading: boolean; results: string[]; value: string }
    >;
    formValues: User;
    selectedAddress: Address;
    setMap: (map: H.Map) => void;
    selectAddress: (address: Address) => void;
    setSelectedAddress: (address: Address) => void;
    mapClick: (e: MapEvent) => void;
}

const userValidation = validate({
    address: {
        street: v(isRequired()),
        houseNumber: v(isRequired(), isNumber()),
        plz: v(isRequired()),
        city: v(isRequired()),
        country: v(isRequired()),
    },
});

function mapUserToForm(user: User) {
    if (!user || !user.address) return user;

    if (user.address.location && user.address.location.coordinates)
        user = merge(user, {
            address: {
                lat: user.address.location.coordinates[0],
                lng: user.address.location.coordinates[1],
            },
        });

    return merge(user, {
        address: {
            houseNumber: user.address.houseNumber.toString(),
            plz: user.address.plz.toString(),
        },
    });
}

function mapFormToUser(user: User) {
    if (!user || !user.address) return user;

    if (user.address.lat && user.address.lng) {
        user = merge(user, {
            address: {
                type: "main",
                location: {
                    type: "Point",
                    coordinates: [user.address.lat, user.address.lng],
                },
            },
        });

        delete user.address.lat;
        delete user.address.lng;
    }

    return user;
}

const enhance: ComponentEnhancer<ProfileInformationProps, any> = compose(
    connect(
        ({
            requests: {
                updateUser: { status: updateUserStatus },
                getLoggedInUser: { status: getLoggedInUserStatus },
            },
            ...state,
        }) => ({
            loading:
                isPending(updateUserStatus) || isPending(getLoggedInUserStatus),
            formValues: getFormValues("profile_personal_information")(state),
        }),
    ),
    reduxForm({
        form: "profile_personal_information",
        validate: userValidation,
        onSubmit: (values: User, dispatch) =>
            dispatch(updateUser(mapFormToUser(values))),
        onSubmitSuccess: (_, dispatch) => dispatch(getLoggedInUser()),
    }),
    withState("map", "setMap", null),
    withState("selectedAddress", "setSelectedAddress", null),
    withState("loadingLocation", "setLoadingLocation", false),
    withState("loadingFields", "setLoadingFields", {}),
    withProps(({ change, setSelectedAddress }) => ({
        updateForm: (values: Location) => {
            change("address.street", values.street);
            change("address.houseNumber", values.houseNumber);
            change("address.plz", values.plz);
            change("address.city", values.city);
            change("address.state.state", values.state);
            change("address.country.state", values.country);
        },
        selectAddress: (address: Address) => {
            change("address.lat", address.lat);
            change("address.lng", address.lng);

            setSelectedAddress(address);
        },
    })),
    lifecycle<ProfileInformationProps, {}>({
        componentWillReceiveProps(nextProps) {
            if (
                !this.props.selectedAddress &&
                this.props.initialValues &&
                this.props.initialValues.address
            ) {
                nextProps.selectAddress(nextProps.initialValues.address);
            }
        },
    }),
    withHandlers({
        mapClick: ({ selectAddress, setLoadingLocation, updateForm }) => (
            e: MapEvent,
        ) => {
            const { lat, lng } = e.context.map.screenToGeo(
                (e.currentPointer as any).viewportX,
                (e.currentPointer as any).viewportY,
            );

            const service = getPlatform().getGeocodingService();
            const params: H.service.ServiceParameters = {
                prox: [lat, lng, "150"].join(","),
                mode: "retrieveAddresses",
                maxresults: "1",
                jsonattributes: "1",
            };

            const setLocation = (address: any, position: any) => {
                const location: Location = {
                    lat: position.latitude,
                    lng: position.longitude,
                    city: address.city,
                    country: address.country,
                    plz: address.postalCode,
                    state: address.state,
                    street: address.street,
                    houseNumber: address.houseNumber,
                };
                updateForm(location);
                selectAddress(location);
            };

            setLoadingLocation(true);

            service.reverseGeocode(
                params,
                (result: H.service.ServiceResult) => {
                    const {
                        address,
                        displayPosition,
                    } = (result.response as any).view[0].result[0].location;

                    setLoadingLocation(false);
                    setLocation(address, displayPosition);
                },
                () => {
                    setLoadingLocation(false);
                },
            );
        },
    }),
);

const PersonalInformationForm = enhance(
    ({
        handleSubmit,
        t,
        error,
        loading,
        mapClick,
        selectAddress,
        selectedAddress,
        setMap,
        formValues,
        change,
    }) =>
        <Form onSubmit={handleSubmit} loading={loading} error={!!error}>
            <Field
                component={GeoServiceField}
                name="address.street"
                fieldName="street"
                categoryFieldName="city"
                type="text"
                context={formValues && formValues.address}
                onResultSelect={(address: Address) => {
                    change("address.street", address.street);
                    selectAddress(address);
                }}
                onResultsReceived={(addresses: Address[]) =>
                    addresses && selectAddress(addresses[0])}
                onSearchChange={(value: string) =>
                    change("address.street", value)}
            />
            <Field
                component={GeoServiceField}
                name="address.houseNumber"
                fieldName="houseNumber"
                categoryFieldName="city"
                type="text"
                context={formValues && formValues.address}
                onResultSelect={(address: Address) => {
                    change("address.houseNumber", address.houseNumber);
                    selectAddress(address);
                }}
                onResultsReceived={(addresses: Address[]) =>
                    addresses && selectAddress(addresses[0])}
                onSearchChange={(value: string) =>
                    change("address.houseNumber", value)}
            />
            <Field
                component={GeoServiceField}
                name="address.plz"
                fieldName="plz"
                categoryFieldName="country"
                type="text"
                context={formValues && formValues.address}
                onResultSelect={(address: Address) => {
                    change("address.plz", address.plz);
                    change("address.city", address.city);
                    selectAddress(address);
                }}
                onResultsReceived={(addresses: Address[]) =>
                    addresses && selectAddress(addresses[0])}
                onSearchChange={(value: string) => change("address.plz", value)}
            />
            <Field
                component={GeoServiceField}
                name="address.city"
                fieldName="city"
                categoryFieldName="country"
                type="text"
                context={formValues && formValues.address}
                onResultSelect={(address: Address) => {
                    change("address.city", address.city);
                    change("address.plz", address.plz);
                    selectAddress(address);
                }}
                onResultsReceived={(addresses: Address[]) =>
                    addresses && selectAddress(addresses[0])}
                onSearchChange={(value: string) =>
                    change("address.city", value)}
            />
            <Field
                component={GeoServiceField}
                name="address.state.state"
                fieldName="state"
                categoryFieldName="country"
                type="text"
                context={formValues && formValues.address}
                onResultSelect={(address: Address) => {
                    change("address.state.state", address.state);
                    selectAddress(address);
                }}
                onResultsReceived={(addresses: Address[]) =>
                    addresses && selectAddress(addresses[0])}
                onSearchChange={(value: string) =>
                    change("address.state.state", value)}
            />
            <Field
                component={GeoServiceField}
                name="address.country.state"
                fieldName="country"
                categoryFieldName="country"
                type="text"
                context={formValues && formValues.address}
                onResultSelect={(address: Address) => {
                    change("address.country.state", address.country);
                    selectAddress(address);
                }}
                onResultsReceived={(addresses: Address[]) =>
                    addresses && selectAddress(addresses[0])}
                onSearchChange={(value: string) =>
                    change("address.country.state", value)}
            />
            {error &&
                <Message
                    error
                    header={t("form-errors")}
                    content={<ErrorList t={t} error={error} />}
                />}
            <Form.Button>
                <Icon name="edit" />Update
            </Form.Button>
            <HereMap
                tap={mapClick}
                onInit={setMap}
                center={
                    selectedAddress && {
                        lat: selectedAddress.lat,
                        lng: selectedAddress.lng,
                    }
                }
            >
                {selectedAddress &&
                    <Marker
                        bitmap="https://locations.michaelkors.com/images/place.svg"
                        lat={selectedAddress.lat}
                        lng={selectedAddress.lng}
                    />}
            </HereMap>
        </Form>,
);

export default ({ user, t }: InjectedTranslateProps & { user: User }) =>
    <div>
        <Header>
            {t("profile.general-header")}
        </Header>
        <PersonalInformationForm initialValues={mapUserToForm(user)} t={t} />
    </div>;
