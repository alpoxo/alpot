import * as React from "react";
import { Menu, Segment, Grid, Rail, Container } from "semantic-ui-react";
import { compose, ComponentEnhancer } from "recompose";
import { connect } from "react-redux";
import { User } from "../../models/user";
import { identity } from "lodash";
import { translate, InjectedTranslateProps } from "react-i18next";
import { getUser } from "../../services/users/actions";
import {
    Switch,
    Route,
    RouteComponentProps,
    NavLink,
    match,
} from "react-router-dom";
import { capitalize } from "lodash";
import GeneralPart from "./parts/General";
import PersonalInformation from "./parts/PersonalInformation";

interface ProfileProps extends InjectedTranslateProps {
    user: User;
    match: match<any>;
    getUser?: (username: string) => Promise<User>;
}

const enhance: ComponentEnhancer<
    ProfileProps,
    RouteComponentProps<any>
> = compose(
    connect(identity, dispatch => ({
        getUser: username => dispatch(getUser(username)),
    })),
    translate(),
);

const ProfileRouter = ({ match, user, t }: ProfileProps) =>
    <Switch>
        <ProfileRoute
            exact
            path="/profile"
            component={GeneralPart}
            props={{ user, t }}
        />
        <ProfileRoute
            path={`${match.url}/general`}
            component={GeneralPart}
            props={{ user, t }}
        />
        <ProfileRoute
            path={`${match.url}/personal-information`}
            component={PersonalInformation}
            props={{ user, t }}
        />
    </Switch>;

const ProfileRoute = ({ component: Component, props, ...rest }: any) =>
    <Route
        {...rest}
        render={internalProps => <Component {...internalProps} {...props} />}
    />;

const SideBar = ({
    baseUrl,
    t,
}: InjectedTranslateProps & { baseUrl: string }) =>
    <div>
        <Menu vertical fluid>
            <Menu.Item>
                <Menu.Header>
                    {capitalize(t("informations"))}
                </Menu.Header>
                <Menu.Menu>
                    <Menu.Item
                        as={NavLink}
                        to={`${baseUrl}/general`}
                        name={t("general")}
                    />
                    <Menu.Item
                        as={NavLink}
                        to={`${baseUrl}/personal-information`}
                        name={t("personal-information")}
                    />
                </Menu.Menu>
            </Menu.Item>
        </Menu>
    </div>;

export default enhance(({ match, user, t }) =>
    <Container>
        <Grid centered columns={2}>
            <Grid.Column width={10}>
                <Segment>
                    <ProfileRouter match={match} user={user} t={t} />
                    <Rail close position="left">
                        <SideBar baseUrl={match.url} t={t} />
                    </Rail>
                </Segment>
            </Grid.Column>
        </Grid>
    </Container>,
);
