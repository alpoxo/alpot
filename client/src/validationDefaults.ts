import { ValidationOptions } from "./utils/validation";
import i18n from "./i18n";

export default <ValidationOptions>{
    messageTransforms: [(input: string, opts) => i18n.t(input, opts) || input],
    messages: {
        isRequired: "validation.required",
        length: "validation.length",
        passwordComplexity: "validation.password-complexity",
        match: "validation.match",
        email: "validation.email",
        isNumber: "validation.isNumber",
    },
};
