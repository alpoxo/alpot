import { Address } from "./address";

export interface User {
    username: string;
    email: string;
    address: Address;
}
