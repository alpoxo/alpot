import { Point } from "geojson";

// Please keep this in sync to
// /server/migrations/sqls/20170717120902-add-products-up.sql
export enum PRICE_UNIT {
    Hourly = "hourly",
    Amount = "amount",
}

/**
 * Defines the interface for a good
 * resource
 * 
 * @export
 * @interface User
 */
export interface Good {
    id: number;
    name: string;
    description: string;
    price: number;
    price_unit: PRICE_UNIT;
    picture: string;
    location: Point;
    created_at?: Date;
    updated_at?: Date;
}

/**
 * Defines the interface for a product
 * resource
 * 
 * @export
 * @interface User
 */
export interface Product extends Good {
    amount: number;
    origin: string;
    isFood: boolean;
}
