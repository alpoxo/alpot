type GeoPoint = {
    type: "Point";
    coordinates: number[];
};

export interface Address {
    street: string;
    plz: string;
    city: string;
    country: string;
    state: string;
    houseNumber: number;
    type: string;
    location: GeoPoint;
    lat: number;
    lng: number;
    [key: string]: string | number | GeoPoint;
}
