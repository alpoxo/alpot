import { combineReducers } from "redux";
import { userReducer, authReducer } from "./services/authentication/reducer";
import { reducer as formReducer } from "redux-form";
import { all, call } from "redux-saga/effects";
import { requestReducer, requestSaga } from "./services/index";
import { shopReducer } from "./pages/shop/reducer";

export const rootReducer = combineReducers({
    form: formReducer,
    user: userReducer,
    auth: authReducer,
    requests: requestReducer,
    shop: shopReducer,
});

export function* rootSaga() {
    yield all([call(requestSaga)]);
}
