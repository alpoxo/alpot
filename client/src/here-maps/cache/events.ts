const Events = new Map<any, Map<string, Function>>();

export function registerEvent(
    context: any,
    target: any,
    type: string,
    eventHandler: Function,
) {
    let targetEvents = Events.get(target);

    if (!targetEvents) {
        targetEvents = new Map<string, Function>();
        Events.set(target, targetEvents);
    }

    const previousEvent = targetEvents.get(type);
    if (previousEvent) target.removeEventListener(type, <any>previousEvent);

    const newEvent = (e: any) => {
        e.context = context;

        const originalEventHandler = eventHandler;
        originalEventHandler(e);
    };
    target.addEventListener(type, newEvent);
    targetEvents.set(type, newEvent);
}
