let platformCache: H.service.Platform = null;

export function getPlatform() {
    return platformCache;
}

export function initializePlatform(app_id: string, app_code: string) {
    platformCache = new H.service.Platform({
        app_id,
        app_code,
    });
}
