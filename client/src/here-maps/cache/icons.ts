type IconKey = string | HTMLImageElement | HTMLCanvasElement;
const Icons = new Map<IconKey, H.map.Icon>();

export default function getIcon(key: IconKey): H.map.Icon {
    if (Icons.has(key)) return Icons.get(key);
    const icon = new H.map.Icon(key);
    Icons.set(key, icon);
    return icon;
}
