type IconKey = string | Element;
const Icons = new Map<IconKey, H.map.DomIcon>();

export default function getDomIcon(html: IconKey): H.map.DomIcon {
    if (Icons.has(html)) return Icons.get(html);
    const icon = new H.map.DomIcon(html);
    Icons.set(html, icon);
    return icon;
}
