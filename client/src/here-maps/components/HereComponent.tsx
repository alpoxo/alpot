import { registerEvent } from "../cache/events";

export const events = [
    "pointerup",
    "pointermove",
    "pointerenter",
    "pointerleave",
    "pointercancel",
    "dragstart",
    "drag",
    "dragend",
    "tap",
    "dbltap",
];

export interface MapContext {
    map: H.Map;
}

export interface MapEvent extends H.mapevents.Event {
    context: MapContext;
}

export type EventHandler = (event: MapEvent) => void;

export interface HereComponentEventProps {
    pointerup?: EventHandler;
    pointermove?: EventHandler;
    pointerenter?: EventHandler;
    pointerleave?: EventHandler;
    pointercancel?: EventHandler;
    dragstart?: EventHandler;
    drag?: EventHandler;
    dragend?: EventHandler;
    tap?: EventHandler;
    dbltap?: EventHandler;
}

export function EventMixin(
    context: MapContext,
    target: H.util.EventTarget,
    props: HereComponentEventProps,
) {
    return Object.keys(props)
        .filter(key => events.indexOf(key) !== -1)
        .forEach((key: string) => {
            registerEvent(context, target, key, (props as any)[key]);
        }, {});
}
