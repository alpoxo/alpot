import * as React from "react";
import { EventMixin, HereComponentEventProps, MapContext } from "./HereComponent";
import { getPlatform } from "../cache/platform";

interface HereMapProps extends HereComponentEventProps {
    zoom?: number;
    center?: H.geo.IPoint;
    layerSelector?: (layers: H.service.DefaultLayers) => H.map.layer.TileLayer;
    onInit?: (map: H.Map) => void;
}

export class HereMap extends React.Component<HereMapProps>
    implements React.ChildContextProvider<MapContext> {

    mapContainer: HTMLDivElement;
    map: H.Map;
    layers: H.service.DefaultLayers;
    behavior: H.mapevents.Behavior;
    mapEvents: H.mapevents.MapEvents;

    constructor(props: HereMapProps, context: object) {
        super(props, context);
    }

    public static childContextTypes = {
        map: React.PropTypes.object,
        platform: React.PropTypes.object
    };

    getChildContext() {
        return {
            map: this.map
        };
    }

    initialize() {
        this.initializeMap();
        this.initializeMapEvents();
        this.initializeBehavior();

        // Call the init event with the map
        // for that one has the chance to save
        // the map to another state
        if(this.props.onInit)
            this.props.onInit(this.map);
    }

    initializeMap() {
        // Obtain the default map types from the platform object:
        this.layers = getPlatform().createDefaultLayers();

        const layerSelector =
            this.props.layerSelector ||
            ((layers: H.service.DefaultLayers) => layers.normal.map);

        const layer = layerSelector(this.layers);

        const mapOptions = {
            zoom: this.props.zoom || 5,
            center: this.props.center || { lat: 52.5, lng: 52.5 },
        };

        // Instantiate (and display) a map object:
        this.map = new H.Map(this.mapContainer, layer, mapOptions);
        return this.map;
    }

    initializeMapEvents() {
        // Enable the event system on the map instance:
        this.mapEvents = new H.mapevents.MapEvents(this.map);
        return this.mapEvents;
    }

    initializeBehavior() {
        // Instantiate the default behavior, providing the mapEvents object:
        this.behavior = new H.mapevents.Behavior(this.mapEvents);
    }

    addLayer(
        layerSelector: (layer: H.service.DefaultLayers) => H.map.layer.TileLayer;
    ) {
        const layer = layerSelector(this.layers);
        this.map.addLayer(layer);
        return layer;
    }

    componentWillUpdate(nextProps: HereMapProps) {
        EventMixin(this.getChildContext(), this.map, nextProps);
    }

    componentDidMount() {
        this.initialize();
    }

    componentWillReceiveProps(nextProps: HereMapProps) {
        if(nextProps.zoom !== this.props.zoom)
            this.map.setZoom(nextProps.zoom);

        if(nextProps.center && nextProps.center !== this.props.center)
            this.map.setCenter(nextProps.center);
    }

    render() {
        return (
            <div style={{width: "640px", height: "480px"}} ref={container => (this.mapContainer = container)}>
                {this.props.children}
            </div>
        );
    }
}