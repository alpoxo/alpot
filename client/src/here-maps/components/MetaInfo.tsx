import * as React from "react";
import {
    HereComponentEventProps,
    EventMixin,
    MapContext,
} from "./HereComponent";
import { getPlatform } from "../cache/platform";

interface MetaInfoProps extends HereComponentEventProps {}

export class MetaInfo extends React.Component<MetaInfoProps> {
    provider: H.map.provider.Provider;
    layer: H.map.layer.Layer;

    public context: MapContext;

    public static contextTypes = {
        map: React.PropTypes.object,
        platform: React.PropTypes.object,
    };

    componentWillUpdate(nextProps: MetaInfoProps) {
        EventMixin(this.context, this.provider, nextProps);
    }

    componentWillUnmount() {
        this.context.map.removeLayer(this.layer);
    }

    render(): null {
        const layers = getPlatform().createDefaultLayers();
        this.layer = (layers.normal as any).metaInfo;
        this.context.map.addLayer(this.layer);
        return null;
    }
}
