import * as React from "react";
import * as ReactDOMServer from "react-dom/server";
import {
    HereComponentEventProps,
    EventMixin,
    MapContext,
} from "./HereComponent";
import getDomIcon from "../cache/domIcons";
import getIcon from "../cache/icons";

type IconType = string | HTMLImageElement | HTMLCanvasElement;
type BitmapType = IconType | React.Component;

interface MarkerProps
    extends HereComponentEventProps,
        H.map.Marker.Options,
        H.geo.IPoint {
    bitmap?: BitmapType;
}

export class Marker extends React.Component<MarkerProps, object> {
    marker: H.map.Marker | H.map.DomMarker;

    public context: MapContext;

    public static contextTypes = {
        map: React.PropTypes.object,
    };

    public setPosition(position: H.geo.IPoint) {
        this.marker.setPosition(position);
    }

    public componentWillReceiveProps(nextProps: MarkerProps) {
        if (
            nextProps.lat !== this.props.lat ||
            nextProps.lng !== this.props.lng
        ) {
            this.setPosition({
                lat: nextProps.lat,
                lng: nextProps.lng,
            });
        }
    }

    componentWillUpdate(nextProps: MarkerProps) {
        EventMixin(this.context, this.marker, nextProps);
    }

    componentWillUnmount() {
        const map = this.context.map;

        if (this.marker) map.removeObject(this.marker);
    }

    addMarker() {
        const { bitmap: iconFromProps, children, lat, lng } = this.props;

        const markerOptions: H.map.Marker.Options = {
            min: this.props.min,
            max: this.props.max,
            visibility:
                this.props.visibility === undefined
                    ? true
                    : this.props.visibility,
            zIndex: this.props.zIndex,
            provider: this.props.provider,
            icon: this.props.icon,
            data: this.props.data,
        };

        if (React.Children.count(children) > 0) {
            const iconMarkup = ReactDOMServer.renderToStaticMarkup(
                <div>
                    {children}
                </div>,
            );

            const icon = getDomIcon(iconMarkup);
            this.marker = new H.map.DomMarker(
                { lat, lng },
                { ...markerOptions, icon },
            );
        } else if (iconFromProps) {
            this.marker = new H.map.Marker(
                { lat, lng },
                { ...markerOptions, icon: getIcon(iconFromProps as IconType) },
            );
        } else this.marker = new H.map.Marker({ lat, lng }, markerOptions);

        this.context.map.addObject(this.marker);
    }

    render(): null {
        const map = this.context.map;

        if (map && !this.marker) this.addMarker();

        return null;
    }
}
