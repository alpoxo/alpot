
CREATE TYPE price_unit AS ENUM
('hourly', 'amount');

CREATE TABLE IF NOT EXISTS goods
(
    id serial primary key,
    name text not null,
    description text not null,
    picture text,
    price decimal not null,
    price_unit price_unit,
    location GEOGRAPHY(POINT, 4326),
    user_id int references users (id),
    created_at timestamp,
    updated_at timestamp
);