/* Replace with your SQL commands */

CREATE TABLE IF NOT EXISTS products
(
    id serial primary key,
    amount int,
    is_food boolean,
    origin text
);