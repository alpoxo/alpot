/* Replace with your SQL commands */

CREATE TABLE users
(
    id serial primary key,
    username text not null,
    password text not null,
    emailId int references emails(id),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    jwt_token text,
    jwt_expiration_date TIMESTAMP,
    verification_key text,
    verified boolean,
    is_trader boolean,
    addressId int references addresses(id)
)