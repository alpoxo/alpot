/* Replace with your SQL commands */

CREATE TABLE states
(
    id serial primary key,
    state char(5),
    description text
);

CREATE TABLE countries
(
    id serial primary key,
    country char(5),
    description text
);

CREATE TABLE phones
(
    id serial primary key,
    number text not null,
    type text
);

CREATE TABLE emails
(
    id serial primary key,
    email text not null,
    type text
);

CREATE TABLE addresses
(
    id serial primary key,
    name text not null,
    street text not null,
    plz int not null,
    city text not null,
    stateId int references states(id),
    countryId int references countries(id)
);

CREATE TABLE address_phones
(
    addressId int references addresses(id),
    phoneId int references phones(id)
);

CREATE TABLE address_email
(
    addressId int references addresses(id),
    emailId int references emails(id)
);