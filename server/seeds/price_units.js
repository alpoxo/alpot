exports.seed = function(knex, Promise) {
    // Deletes ALL existing entries
    return knex("price_units").del().then(function() {
        // Inserts seed entries
        return knex("price_units").insert([
            { id: 1, unit: "amount", description: "An amount of items" },
        ]);
    });
};
