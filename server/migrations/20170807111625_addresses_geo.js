exports.up = function(knex, Promise) {
    return knex.schema.alterTable("addresses", function(table) {
        table.specificType("location", "GEOGRAPHY(POINT, 4326)");
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.alterTable("addresses", function(t) {
        table.dropColumn("location");
    });
};
