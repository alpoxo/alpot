exports.up = function(knex, Promise) {
    return knex.schema.createTable("users_addresses", function(table) {
        table.integer("user_id").references("id").inTable("users");
        table.integer("address_id").references("id").inTable("addresses");
        table.primary(["user_id", "address_id"]);
        table.timestamps();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable("users_addresses");
};
