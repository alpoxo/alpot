/*
CREATE TABLE users
(
    id serial primary key,
    username text not null,
    password text not null,
    emailId int references emails(id),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    jwt_token text,
    jwt_expiration_date TIMESTAMP,
    verification_key text,
    verified boolean,
    is_trader boolean,
    addressId int references addresses(id)
)
*/

exports.up = function(knex, Promise) {
    return knex.schema.createTable("users", function(table) {
        table.increments("id").primary();
        table.string("username").notNullable();
        table.string("password").notNullable();
        table.text("jwt_token");
        table.timestamp("jwt_expiration_date").defaultTo(knex.fn.now());
        table.string("verification_key");
        table.boolean("verified");
        table.boolean("is_trader");
        table.timestamps();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable("users");
};
