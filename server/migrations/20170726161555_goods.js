/*
CREATE TABLE IF NOT EXSISTS price_units
(
    id serial primary key,
    unit text,
    description text
);

CREATE TABLE IF NOT EXISTS goods
(
    id serial primary key,
    name text not null,
    description text not null,
    picture text,
    price decimal not null,
    price_unit price_unit,
    location GEOGRAPHY(POINT, 4326),
    user_id int references users (id),
    amount int,
    is_food boolean,
    origin text,
    created_at timestamp,
    updated_at timestamp
);
*/
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable("price_units", function(table) {
            table.increments("id").primary();
            table.string("unit");
            table.text("description");
        })
        .createTable("products", function(table) {
            table.increments("id").primary();
            table.string("name").notNullable();
            table.text("description").notNullable();
            table.string("picture");
            table.decimal("price").notNullable();
            table
                .integer("price_unit_id")
                .references("id")
                .inTable("price_units")
                .notNullable();
            table.specificType("location", "GEOGRAPHY(POINT, 4326)");
            table
                .integer("user_id")
                .references("id")
                .inTable("users")
                .notNullable();
            table.integer("amount").defaultTo(1);
            table.boolean("is_food");
            table.integer("state_id").references("id").inTable("states");
            table.string("origin");
            table.timestamps();
        });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable("products").dropTable("price_units");
};
