/*
CREATE TABLE states
(
    id serial primary key,
    state char(5),
    description text
);

CREATE TABLE countries
(
    id serial primary key,
    country char(5),
    description text
);

CREATE TABLE phones
(
    id serial primary key,
    number text not null,
    type text
);

CREATE TABLE emails
(
    id serial primary key,
    email text not null,
    type text
);

CREATE TABLE addresses
(
    id serial primary key,
    name text not null,
    street text not null,
    plz int not null,
    city text not null,
    stateId int references states(id),
    countryId int references countries(id)
);

CREATE TABLE address_phones
(
    addressId int references addresses(id),
    phoneId int references phones(id)
);

CREATE TABLE address_email
(
    addressId int references addresses(id),
    emailId int references emails(id)
);
*/

exports.up = function(knex, Promise) {
    return knex.schema
        .createTable("states", function(table) {
            table.increments("id").primary();
            table.string("state", 5).notNullable();
            table.text("description");
            table.timestamps();
        })
        .createTable("countries", function(table) {
            table.increments("id").primary();
            table.string("state", 5).notNullable();
            table.text("description");
            table.timestamps();
        })
        .createTable("phones", function(table) {
            table.increments("id").primary();
            table.string("number").notNullable();
            table.text("type");
            table.timestamps();
        })
        .createTable("emails", function(table) {
            table.increments("id").primary();
            table.integer("owner_id").notNullable();
            table.string("email").notNullable();
            table.text("type");
            table.timestamps();
        })
        .createTable("addresses", function(table) {
            table.increments("id").primary();
            table.string("name");
            table.string("street").notNullable();
            table.integer("houseNumber").notNullable();
            table.integer("plz").notNullable();
            table.string("city").notNullable();
            table.string("type").notNullable();
            table.integer("state_id").references("id").inTable("states");
            table.integer("country_id").references("id").inTable("countries");
            table.timestamps();
        })
        .createTable("addresses_phones", function(table) {
            table.integer("addresses_id").references("id").inTable("addresses");
            table.integer("phones_id").references("id").inTable("phones");
            table.primary(["addresses_id", "phones_id"]);
            table.timestamps();
        })
        .createTable("addresses_emails", function(table) {
            table.integer("addresses_id").references("id").inTable("addresses");
            table.integer("emails_id").references("id").inTable("emails");
            table.primary(["addresses_id", "emails_id"]);
            table.timestamps();
        });
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable("addresses_emails")
        .dropTable("addresses_phones")
        .dropTable("addresses")
        .dropTable("emails")
        .dropTable("phones")
        .dropTable("countries")
        .dropTable("states");
};
