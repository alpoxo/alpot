/**
 * Configures the knex database
 * access
 *
 * @module util/resources.ts
 * @author Elias Bernhaut
 */

const config = require("../knexfile");
import * as knexInit from "knex";
import * as objection from "objection";

const knex = knexInit(config[process.env.NODE_ENV]);
objection.Model.knex(knex);

export const dbMock = async () => {
    await knex.migrate.latest();
};
