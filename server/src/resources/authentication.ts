import authentication from "../auth/authentication";
import { composeRouter, withPost, withGet, before } from "../util/resources";

const { registration, login, getCurrentUser } = authentication;

export default composeRouter("auth")(
    withPost("/register", registration)(),
    withPost("/login", login)(),
    withGet("/current", getCurrentUser)(before(authentication.authenticate)),
);
