/**
 * Defines the resources for the users
 *
 * @module resources/users.ts
 * @author Elias Bernhaut
 */

import {
    composeRouter,
    before,
    withMiddleware,
    withGet,
    PreBuiltResource,
} from "../util/resources";
import { defaults } from "../util/default_resources";
import authentication from "../auth/authentication";
import Product from "../models/Product";
import { Context } from "koa";
import { compose, omit, values } from "ramda";

const RESOURCE_NAME = "products";
const defaultResources = defaults(Product);
const restResources = <PreBuiltResource[]>compose(values, omit(["create"]))(
    defaultResources,
);

const enhanceWithUserId = async (ctx: Context, next: () => Promise<any>) => {
    console.log(ctx.state);
    ctx.request.body.user_id = ctx && ctx.state && ctx.state.user.id;
    await next();
};

export default composeRouter(RESOURCE_NAME)(
    // The creation has to be enhanced with a userid
    // for reference
    defaultResources.create(
        before(authentication.authenticate, enhanceWithUserId),
    ),
    // Add the rest of the default resources anyway
    withMiddleware(...restResources)(before(authentication.authenticate)),
    // Get a product specifically for a user
    withGet("/user/:username", async (ctx, next) => {
        const username = ctx.params.username;
        const product = await Product.query()
            .joinRelation("user")
            .select("products.*")
            .where("user.username", username);

        ctx.body = product;
        ctx.status = 200;
        return next();
    })(before(authentication.authenticate)),
);
