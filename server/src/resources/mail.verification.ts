import { composeRouter, withGet } from "../util/resources";
import { emailVerificationRoute } from "../config/configuration";
import authentication from "../auth/authentication";

const { verifyMail } = authentication;

export default composeRouter(emailVerificationRoute)(
    withGet("/:verification_key", verifyMail)(),
);
