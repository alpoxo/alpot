/**
 * Defines the resources for the users
 *
 * @module resources/users.ts
 * @author Elias Bernhaut
 */

import { composeRouter, before } from "../util/resources";
import { withDefaults } from "../util/default_resources";
import authentication from "../auth/authentication";
import User from "../models/User";

const RESOURCE_NAME = "users";

export default composeRouter(RESOURCE_NAME)(
    withDefaults(User)(before(authentication.authenticate)),
);
