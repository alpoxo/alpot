/**
 * This is a module for sending mails.
 * Nothing more.
 *
 * @module util/resources.ts
 * @author Elias Bernhaut
 */

import { config } from "dotenv";
import * as path from "path";
import { EmailTemplate } from "email-templates";
import * as nodemailer from "nodemailer";
import { Transporter, SendMailOptions } from "nodemailer";
import User from "./models/User";
import { map, path as ramdaPath, zip } from "ramda";

// Configure the dotenv environment variables
config();

// create reusable transporter object using the default SMTP transport
const transportDefaults = {
    host: process.env.MAIL_SMTP,
    port: 465,
    secure: true, // secure:true for port 465, secure:false for port 587
    auth: {
        user: process.env.MAIL_FROM,
        pass: process.env.MAIL_PASSWORD,
    },
};

/**
 * An error specifically for mail errors
 * 
 * @export
 * @class MailSendError
 * @extends {Error}
 */
export class MailSendError extends Error {
    /**
     * Creates an instance of MailSendError.
     * 
     * @param {string} message The error message
     * @param {Error} [internalError] An internal error, if any
     * @memberof MailSendError
     */
    constructor(
        public message: string,
        public users: User[],
        public mailOptions: SendMailOptions,
        public internalError?: Error,
    ) {
        super(message);
        Error.captureStackTrace(this, this.constructor);
        this.name = "MailSendError";
    }
}

/**
 * An interface for the options object
 * passed to sendMail.
 * 
 * @interface MailOptions
 */
export interface MailOptions {
    subject?: string;
    text?: string;
    html?: string;
}

/**
 * A class which is responsible for sending mails.
 * Thats it.
 * 
 * @export
 * @class Mailer
 */
export class Mailer {
    constructor(
        private transporter: Transporter = nodemailer.createTransport(
            transportDefaults,
        ),
        private emailTemplate: new (
            template: string,
        ) => EmailTemplate = EmailTemplate,
    ) {}

    /**
     * Renders the specified email-template with the
     * given user
     * 
     * @param {string} template The email-template which should
     *  be rendered
     * @param {User} user The user which should be given to the
     *  template as context
     * @returns {Promise<MailOptions>} 
     */
    private async renderTemplate(
        template: string,
        user: User,
        additionalOptions: object,
    ): Promise<MailOptions> {
        const failedError = `Failed to render the mail template for user ${user.username}`;
        const templateDir = path.join(__dirname, "..", "templates", template);

        // Build the email-renderer
        const renderer = new this.emailTemplate(templateDir);

        // Render the template
        // Inject environment variables and
        // Inject additionalOptions
        const result = await renderer.render({
            ...user,
            ...additionalOptions,
            env: process.env,
        });

        // Reject promise if no result came back -
        // The return type void of the promise might be an error
        // in the typings - ??
        if (!result) throw new Error(failedError);

        return result;
    }

    /**
     * Sends a mail to the given users.
     * 
     * @template T 
     * @param {MailOptions} options An object containing
     *  the subject, the text and the html to send.
     *  the text and the subject are optional.
     * @param {...T[]} users A listing of users to
     *  which to send the mail
     * @returns {Promise<void>} A promise resolving when the
     *  mail(s) are/is sent successfully
     * @memberof Mailer
     */
    async sendMail<T extends User>(
        options: MailOptions,
        ...users: T[]
    ): Promise<void> {
        // Select needed environment variables
        const [user, from] = [process.env.MAIL_NAME, process.env.MAIL_FROM];

        // Create final mail options from the options given
        const mailOptions = {
            from: `${user} <${from}>`,
            to: map(ramdaPath(["email", "email"]), users).join(", "),
            ...options,
        };

        // send mail with defined transport object
        try {
            await this.transporter.sendMail(mailOptions);
        } catch (error) {
            throw new MailSendError(
                "A mail could not be sent: " + error.message,
                users,
                mailOptions,
                error,
            );
        }
    }

    /**
     * Sends a templated email to each user.
     * The template is chosen from /templates/<template>
     * where <template> is substituted with the string given
     * in the parameter template.
     * 
     * @template T 
     * @param {string} template The template which is to be
     *  rendered for each user
     * @param {...T[]} users The users to which the emails
     *  should be sent
     * @returns {Promise<void>} A promise resolving when the
     *  mail(s) are/is sent successfully
     * @memberof Mailer
     */
    async sendMailTemplate<T extends User>(
        template: string,
        additionalOptions: object,
        ...users: T[]
    ): Promise<void> {
        if (!users || !users.length) return; // Nothing to do

        // Render all the templates
        const templates = map(
            user => this.renderTemplate(template, user, additionalOptions),
            users,
        );
        const optionArray = await Promise.all(templates);

        // Send a mail to each user
        const mapMailSendToPromise = map(([option, user]) =>
            this.sendMail(option, user),
        );
        const mailPromises = mapMailSendToPromise(zip(optionArray, users));

        await Promise.all(mailPromises);
    }
}

// By default, export the default mailer :-)
export default new Mailer(nodemailer.createTransport(transportDefaults));
