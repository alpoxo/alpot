import { Mailer } from "./mailer";
import { EmailTemplate } from "email-templates";
import { Transporter } from "nodemailer";

export const EmailTemplateMock = jest.fn<EmailTemplate>(() => ({
    render: () => ({ html: "you got your html" }),
}));

export const TransporterMock = jest.fn<Transporter>(() => ({
    sendMail: jest.fn(() =>
        Promise.resolve({
            messageId: "testMessage",
            response: {},
        }),
    ),
}));

export const MailerMock = jest.fn<Mailer>(() => ({
    sendMail: jest.fn(() => Promise.resolve()),
    sendMailTemplate: jest.fn(() => Promise.resolve()),
}));
