import CRUD from "./crud";
import { IDatabase } from "pg-promise";

export const DatabaseMock = jest.fn<IDatabase<any>>(() => ({
    one: jest.fn(() => Promise.resolve({})),
    oneOrNone: jest.fn(() => Promise.resolve()),
    many: jest.fn(() => Promise.resolve([{}])),
    manyOrNone: jest.fn(() => Promise.resolve()),
    query: jest.fn(() => Promise.resolve({})),
}));

export default jest.fn<CRUD<any>>(() => ({
    database: new DatabaseMock(),
    all: jest.fn(() => []),
    find: jest.fn(async () => ({})),
    create: jest.fn(async () => void {}),
    update: jest.fn(async () => void {}),
    delete: jest.fn(async () => void {}),
    exists: jest.fn(async () => true),
    existsByKey: jest.fn(async () => ({})),
}));
