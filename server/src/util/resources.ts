/**
 * Util for koa - define resources
 * in a composable way.
 * @module util/resources.ts
 * @author Elias Bernhaut
 */

import { compose } from "./compose";
import * as Router from "koa-router";
import "koa-bodyparser";

export type Hook = (hook: Hooks) => Hooks;
export type HookBuilder = (...middleware: Router.IMiddleware[]) => Hook;

export interface Hooks {
    before: Router.IMiddleware[];
    after: Router.IMiddleware[];
}

export type PreBuiltResource = (...hooks: Hook[]) => (router: Router) => Router;

export type ResourceBuilder = (
    path: string,
    middleware: Router.IMiddleware,
) => PreBuiltResource;

/**
 * withMethod is a curried function for generating routes
 *
 * @param {string} method - An http method
 */
export type withMethod = (method: string) => ResourceBuilder;
export const withMethod: withMethod = method => (path, middleware) => (
    ...hooks
) => router => {
    // Compose the hooks of one route
    const composedHooks: Hooks = compose(...hooks)({
        after: [],
        before: [],
    });

    // Concat the hooks with the main callback
    const finalHooks = composedHooks.before
        .concat([middleware])
        .concat(composedHooks.after);

    // Finally register the route
    // And return the router
    const anyRouter: any = router;
    anyRouter[method](path, ...finalHooks);
    return router;
};

/**
 * Create koa-router middleware for the use with
 * composeRouter.
 *
 * Defines a route for get-requests.
 *
 * @param {string} path - The path for router layer
 * @param {IMiddleware} middleware - The middleware which is to apply
 */
export const withGet = withMethod("get");

/**
 * Create koa-router middleware for the use with
 * composeRouter.
 *
 * Defines a route for post-requests.
 *
 * @param {string} path - The path for router layer
 * @param {IMiddleware} middleware - The middleware which is to apply
 */
export const withPost = withMethod("post");

/**
 * Create koa-router middleware for the use with
 * composeRouter.
 *
 * Defines a route for delete-requests.
 *
 * @param {string} path - The path for router layer
 * @param {IMiddleware} middleware - The middleware which is to apply
 */
export const withDelete = withMethod("delete");

/**
 * Create koa-router middleware for the use with
 * composeRouter.
 *
 * Defines a route for put-requests.
 *
 * @param {string} path - The path for router layer
 * @param {IMiddleware} middleware - The middleware which is to apply
 */
export const withPut = withMethod("put");

/**
 * Create koa-router middleware for the use with
 * composeRouter.
 *
 * Defines a route for patch-requests.
 *
 * @param {string} path - The path for router layer
 * @param {IMiddleware} middleware - The middleware which is to apply
 */
export const withPatch = withMethod("patch");

/**
 * Builds a before-hook for the use with composeRouter.
 * @example <caption>Example usage: </caption>
 * <pre><code>
 * composeRouter("fooRoute")(
 *  withGet("/", ctx => ctx.body = "foo") (
 *    before(
 *      ctx => ctx.body ="bar";
 *    )
 *  )
 * )
 * </code></pre>
 * @param {IMiddleware[]} middleware - One or more middleware functions
 */
export const before: HookBuilder = (...middleware) => hook => {
    hook.before.push(...middleware);
    return hook;
};

/**
 * Builds a before-hook for the use with composeRouter.
 * @example <caption>Example usage: </caption>
 * <pre><code>
 * composeRouter(
 *  withGet("/", ctx => ctx.body = "foo") (
 *    after(
 *      ctx => ctx.body ="bar";
 *    )
 *  )
 * )
 * </code></pre>
 * @param {IMiddleware[]} middleware - One or more middleware functions
 */
export const after: HookBuilder = (...middleware) => hook => {
    hook.after.push(...middleware);
    return hook;
};

export type ComposeFunction = (...args: any[]) => any;

export type composeRouter = (
    name?: string,
) => (...funcs: ComposeFunction[]) => Router;

/**
 * Composes a router from multiple middleware functions
 * <pre><code>
 * composeRouter("user")(
 *  withDefaults("user");
 * )
 * </code></pre>
 * @param {string?} name - The name of the route.
 *                         Also used as
 */
export const composeRouter: composeRouter = name => (...funcs) => {
    const router: Router = compose(...funcs)(
        new Router({
            prefix: `/${name}`,
        }),
    );
    return router;
};

/**
 * Composes multiple resources to one resource.
 * The applied hooks will be applied to all of the
 * resources.
 * 
 * @param middleware A list of middleware functions
 * 
 */
export const withMiddleware = (...middleware: PreBuiltResource[]) => (
    ...hooks: Hook[]
) => compose(...middleware.map((def: Function) => def(...hooks)));
