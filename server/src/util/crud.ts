/**
 * Util for the use with pg-promise.
 * A class which provides CRUD operations
 * through the pg-promise interface
 *
 * @module util/crud.ts
 * @author Elias Bernhaut
 */
import { IDatabase } from "pg-promise";
import { omit, map, keys, any, values, compose } from "ramda";
import db from "../bookshelf";
import * as wkx from "wkx";

/**
 * Transforms strings from camelCase to snake_case
 * 
 * @param {string} value The value to transform
 * @returns {string} The snake_case'd value
 */
function snakeCase(value: string) {
    // aB => a_b
    const matchTransformer = (_: any, p1: string, p2: string) =>
        `${p1}_${p2}`.toLowerCase();

    return value.replace(/([a-z])([A-Z])/g, matchTransformer);
}

/**
 * Transforms strings from snake_case to camelCase
 * 
 * @param {string} value The value to transform
 * @returns {string} The camelCase'd value
 */
function camelCase(value: string) {
    const matchTransformer = (_: any, p1: string, p2: string) =>
        `${p1}${p2.toUpperCase()}`;

    return value.replace(/([a-z])_([a-z])/g, matchTransformer);
}

interface ITimestampRecord {
    created_at?: Date;
    updated_at?: Date;
}

/**
 * An utility class which sets up a CRUD interface
 * to the postgres database through the use of
 * pg-promise
 * 
 * @export
 * @class CRUD
 * @template T 
 */
export default class CRUD<T extends ITimestampRecord & { [key: string]: any }> {
    private db: IDatabase<any> = db;

    constructor(public resourceName: string) {}

    /**
     * Returns the database connection
     * 
     * @readonly
     * @memberof CRUD
     */
    get database() {
        return db;
    }

    /**
     * Sets a new implementation for the databse
     * 
     * @memberof CRUD
     */
    set database(database: IDatabase<any>) {
        this.db = database;
    }

    /**
     * Tests if the given value is valid
     * geojson.
     * 
     * @param value The value to test for if its
     *  a valid geojson value
     */
    private isGeoJSON(value: any) {
        // This can be a lot improved in the future.
        // For now, i hope it will be fine :-)
        return value && value.type;
    }

    /**
     * An internal helper function which creates a list of
     * keys for the use in an insert statement of
     * pg-promise.
     * This returns a list like:
     * ${name}, ${email} etc.
     * From the keys of a resource object.
     * 
     * @private
     * @param {object} obj The resource for which to create
     *          the key-list
     * @returns {string} The list of keys
     * @memberof CRUD
     */
    private createKeyList(obj: { [key: string]: any }) {
        const getKey = (key: string) =>
            // If object has a type, use the ST_GeomFromGeoJSON
            // from postgis to reat the geojson as
            // geometry
            this.isGeoJSON(obj[key])
                ? "ST_GeomFromGeoJSON(${" + key + "})"
                : "${" + key + "}";

        return map(getKey, keys(obj)).join(", ");
    }

    /**form
     * An internal helper function which creates an assignment list
     * for updating records with pg-promise.
     * This creates a list like:
     * name=${name}, email=${email} etc.
     * from a resource object
     * 
     * @private
     * @param {object} obj The resource object for which to create the
     *          assignment list
     * @returns {string} The assignment list
     * @memberof CRUD
     */
    private createAssignmentList(obj: object, sep: string = ", ") {
        return map(
            key => '"' + key + '"' + " = ${" + key + "}",
            keys(obj),
        ).join(sep);
    }

    /**
     * Transforms the keys of the
     * given model to snake-case
     * 
     * @param model The model which keys should be transformed
     */
    protected transformKeysToSnakeCase(model: T) {
        if (!model) return model;
        const transformer: { [key: string]: any } = { ...<object>model };
        for (const modelKey in transformer) {
            const modelValue = transformer[modelKey];

            // transform key to snake-case
            const snakeCaseKey = snakeCase(modelKey);
            if (!model[snakeCaseKey]) {
                delete transformer[modelKey];
                transformer[snakeCaseKey] = modelValue;
            }
        }
        return transformer;
    }

    /**
     * Transforms the keys of the
     * given model to camelCase
     * 
     * @param model The model which keys should be transformed
     */
    protected transformKeysToCamelCase(model: T) {
        if (!model) return model;
        const transformer: { [key: string]: any } = { ...<object>model };
        for (const modelKey in transformer) {
            const modelValue = transformer[modelKey];

            // transform key to camelCase
            const camelCaseKey = camelCase(modelKey);
            if (!model[camelCaseKey]) {
                delete transformer[modelKey];
                transformer[camelCaseKey] = modelValue;
            }
        }
        return transformer;
    }

    /**
     * Create a new entry in the database
     * 
     * @param {T} model The resource which is to create
     * @returns {Promise<T>} A promise returning the newly created entry
     * @memberof CRUD
     */
    async create(model: T, withTimestamps: boolean = true): Promise<T> {
        if (withTimestamps) {
            model.created_at = new Date();
            model.updated_at = new Date();
        }

        model = <T>this.transformKeysToSnakeCase(model);

        for (const modelKey in model) {
            const modelValue = model[modelKey];

            // we have to convert all the geojson objects to
            // wkt format to save it properly in the database for the
            // use with postgis
            if (this.isGeoJSON(modelValue))
                model[modelKey] = wkx.Geometry.parseGeoJSON(modelValue).toWkt();
        }

        const keyList = this.createKeyList(model);

        const query =
            "INSERT INTO " +
            this.resourceName +
            "(${this~}) VALUES (" +
            keyList +
            ") RETURNING *";

        const result = await this.db.one(query, model);

        return <T>this.transformKeysToCamelCase(result);
    }

    /**
     * Updates entries of a model.
     * 
     * @param {T} model The model which is to update
     * @returns {Promise<any>} A promise
     * @memberof CRUD
     */
    update(model: T, withTimestamps: boolean = true): Promise<void> {
        // The model will be updated
        if (withTimestamps) model.updatedAt = new Date();

        let modelToUpdate = omit(["id"], model);
        modelToUpdate = <T>this.transformKeysToSnakeCase(modelToUpdate);
        const assignmentList = this.createAssignmentList(modelToUpdate);

        const query =
            "UPDATE " +
            this.resourceName +
            " SET " +
            assignmentList +
            ' WHERE "id"=${id}';

        model = <T>this.transformKeysToSnakeCase(model);
        return this.db.none(query, model);
    }

    /**
     * Retrieve all entries from the resource from the database
     * 
     * @returns {Promise<T[]>} A promise which resolves with an
     *          array of all the found entries
     * @memberof CRUD
     */
    async all(): Promise<T[]> {
        const models: T[] = await this.db.any(
            "SELECT * FROM $1~",
            this.resourceName,
        );
        return <T[]>models.map(this.transformKeysToCamelCase);
    }

    /**
     * Finds a resource and fetches it from the database
     * 
     * @param {number} id The id of the resource
     * @returns {Promise<T>} A promise resolving with the 
     *          found resource or rejects on error
     * @memberof CRUD
     */
    async find(id: number | T): Promise<T> {
        if (typeof id === "number")
            return this.db.oneOrNone("SELECT * FROM $1~ WHERE id=$2", [
                this.resourceName,
                id,
            ]);

        let model = <T>this.transformKeysToSnakeCase(id);
        const assignmentList = this.createAssignmentList(model, " AND ");

        const query =
            "SELECT * FROM " +
            this.resourceName +
            " WHERE " +
            assignmentList +
            ";";

        const result = await this.db.oneOrNone(query, model);

        return <T>this.transformKeysToCamelCase(result);
    }

    /**
     * Deletes a resource from the database
     * 
     * @param {number} id The id of the resource
     * @returns {Promise<any>} A promise which resolves on success
     * @memberof CRUD
     */
    delete(id: number): Promise<void> {
        return this.db.none("DELETE FROM $1~ WHERE id=$2", [
            this.resourceName,
            id,
        ]);
    }

    /**
     * Runs a transaction to test the existence of a model.
     * Takes a model which holds only the keys for which
     * the database table should be tested against.
     * 
     * @param {T} model The model which holds only the
     *      keys for which the existence should be tested.
     * @returns {Promise<boolean>} A promise resolving to
     *      either true or false depending on whether
     *      one of the given properties resolved to true
     * @memberof CRUD
     */
    exists(model: T): Promise<boolean> {
        model = <T>this.transformKeysToSnakeCase(model);
        return this.existsByKey(model).then(compose(any(Boolean), values));
    }

    /**
     * Runs a transaction to test the database table for
     * the in the model given keys.
     * 
     * @param {T} model The model which holds only the keys
     *     with which the existence of the model should be tested
     * @returns {Promise<{ [K in keyof T]: boolean }>} A promise
     *      which holds all the keys given with the parameter model,
     *      but each of those keys now holds a boolean identifying
     *      whether an entry was found through querying with this
     *      property
     * @memberof CRUD
     */
    existsByKey(model: T): Promise<{ [key: string]: boolean }> {
        model = <T>this.transformKeysToSnakeCase(model);
        const keysToTest = keys(model);

        return this.db
            .tx(t => {
                return t.batch(
                    map(
                        key =>
                            t.oneOrNone(
                                "SELECT 1 as exists FROM $1~ WHERE $2~ = $3",
                                [this.resourceName, key, (<any>model)[key]],
                            ),
                        keysToTest,
                    ),
                );
            })
            .then(queryResult => {
                // Results come back as null if no entry was found.
                // Set the null entries to true or false
                const result: boolean[] = queryResult.map(
                    (res: number | null) => (res && true) || false,
                );

                const found = keysToTest.reduce(
                    (a: { [key: string]: boolean }, key, i) => {
                        a[key] = !!result[i];
                        return a;
                    },
                    {},
                );

                return found;
            });
    }
}
