/**
 * Util for koa - define the default
 * resources for later reuse
 *
 * @module util/default_resources.ts
 * @author Elias Bernhaut
 */

import { values } from "ramda";
import {
    PreBuiltResource,
    withMiddleware,
    withGet,
    withPost,
    withDelete,
    withPatch,
} from "./resources";
import ModelBase from "../models/ModelBase";

/**
 * Build up an object holding default routes.
 * The default routes are created through
 * a name identifier which is the same as
 * the name of the corresponding table in the
 * database.
 *
 * @param {string} crud - The CRUD class instance
 */
interface DefaultResources {
    create: PreBuiltResource;
    findAll: PreBuiltResource;
    findOne: PreBuiltResource;
    remove: PreBuiltResource;
    update: PreBuiltResource;
}
export type defaults = (model: typeof ModelBase) => DefaultResources;
export const defaults: defaults = model => ({
    create: withPost("/", async (ctx, next) => {
        const { body } = ctx.request;
        const result = await model.query().insertGraph(body);
        ctx.body = result;
        ctx.status = 200;
        return next();
    }),
    findAll: withGet("/", async (ctx, next) => {
        const result = await model
            .query()
            .eager(model.defaultEager, model.defaultFilters);
        ctx.body = result;
        return next();
    }),
    findOne: withGet("/:id", async (ctx, next) => {
        const { id } = ctx.params;
        const result = await model
            .query()
            .eager(model.defaultEager, model.defaultFilters)
            .findById(id);
        ctx.body = result;
        ctx.status = 200;
        return next();
    }),
    remove: withDelete("/:id", async (ctx, next) => {
        const { id } = ctx.params;
        await model.query().deleteById(id);
        ctx.status = 200;
        return next();
    }),
    update: withPatch("/:id", async (ctx, next) => {
        const { body } = ctx.request;
        await (model.query() as any).upsertGraph(body);
        ctx.status = 200;
        return next();
    }),
});

/**
 * Applies all defaults to a composeRouter call.
 * @example <caption>Example usage: </caption>
 * <pre><code>
 * composeRouter("user")(
 *  withDefaults("user");
 * )
 * </code></pre>
 * @param {string} name - The name of the resource
 */
export type withDefaults = (model: typeof ModelBase) => PreBuiltResource;
export const withDefaults: withDefaults = model =>
    withMiddleware(...(<PreBuiltResource[]>values(defaults(model))));
