import { Middleware } from "koa";

export const errorHandler: Middleware = async (ctx, next) => {
    try {
        await next();
    } catch (err) {
        const status = err.status;

        console.error(err.message);

        if (status) {
            ctx.status = status;
            ctx.body = {
                error: err.fields || err.message,
            };
        } else {
            ctx.status = 500; // internal server error
            ctx.body = {
                error: "server-unexpected-error",
            };
        }
    }
};
