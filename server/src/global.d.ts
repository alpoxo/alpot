import { User } from "./src/models/user";
import * as Bookshelf from "bookshelf";
import "koa";
import "pg-promise";

// Extend koa"s BaseContext with a db instance
// And a user
declare module "koa" {
    interface BaseContext {
        db?: Bookshelf;
        user?: object;
    }
}
