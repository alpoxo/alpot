import CRUD from "../util/crud";
import * as jwt from "jwt-simple";
import { createTokenExpiration, jwtSecret } from "../config/configuration";

/**
 * Defines the interface for a user
 * resource
 * 
 * @export
 * @interface User
 */
export interface User {
    id?: number;
    username?: string;
    password?: string;
    email?: string;
    jwtToken?: string;
    jwtExpirationDate?: Date;
    createdAt?: Date;
    updatedAt?: Date;
    verificationKey?: string;
    verified?: boolean;
}

export const protectedUserFields = [
    "password",
    "jwtToken",
    "jwtExpirationDate",
    "createdAt",
    "updatedAt",
    "verificationKey",
    "verified",
];

/**
 * Defines the default implementation of the
 * IUserCRUD for the user
 * 
 * @class UserCRUD
 * @extends {CRUD<User>}
 * @implements {IUserCRUD}
 */
export class UserCRUD extends CRUD<User> {
    constructor() {
        super("users");
    }

    async createUserWithToken(user: User) {
        return this.database.tx(async _ => {
            const createdUser = await this.create(user);

            // Encode the created user to a token
            // This also includes the new userid
            createdUser.jwtToken = jwt.encode(
                createdUser,
                jwtSecret,
                "HS256",
                {},
            );

            createdUser.jwtExpirationDate = createTokenExpiration();

            await this.update(createdUser);
        });
    }
}

export const userCRUD = new UserCRUD();
