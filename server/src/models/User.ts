/*
    return knex.schema.createTable("users", function(table) {
        table.increments("id").primary();
        table.string("username").notNullable();
        table.string("password").notNullable();
        table.text("jwt_token");
        table.timestamp("jwt_expiration_date").defaultTo(knex.fn.now());
        table.string("verification_key");
        table.boolean("verified");
        table.boolean("is_trader");
        table
            .integer("email_id")
            .references("id")
            .inTable("emails")
            .notNullable();
        table.integer("address_id").references("id").inTable("addresses");
        table.timestamps();
    });
*/

import { Model, QueryBuilder } from "objection";
import ModelBase from "./ModelBase";
import * as jwt from "jwt-simple";
import { createTokenExpiration, jwtSecret } from "../config/configuration";

export default class User extends ModelBase {
    static tableName = "users";

    id: number;
    username: string;
    password: string;
    jwt_token: string;
    jwt_expiration_date: Date;
    verification_key: string;
    verified: boolean;
    is_trader: boolean;
    created_at: Date;
    updated_at: Date;

    static defaultFilters = {
        onlyMain: (queryBuilder: QueryBuilder<{}>) =>
            queryBuilder.where("type", "main"),
    };

    static defaultEager = "[email(onlyMain), address(onlyMain).[state, country]]";

    static protectedFields = [
        "password",
        "jwtToken",
        "jwtExpirationDate",
        "createdAt",
        "updatedAt",
        "verificationKey",
        "verified",
    ];

    static existsByKey(subset: Record<string, any>): Record<string, boolean> {
        const knex = Model.knex();

        const keyIdentifier = (key: string) => {
            const parts = key.split(".");
            return parts[parts.length - 1];
        };

        const requests = Object.keys(subset).map(key =>
            knex
                .raw(
                    User.query()
                        .joinRelation("email")
                        .where({ [key]: subset[key] })
                        .toString(),
                )
                .wrap("exists(", `) as ${keyIdentifier(key)}`),
        );

        return knex.select(...requests) as any;
    }

    static async createToken(user: Partial<User>) {
        const tokenExpirationDate = createTokenExpiration();
        const exp = tokenExpirationDate.getTime() / 1000;

        const update = {
            // Encode the created user to a token
            jwt_token: jwt.encode({ ...user, exp }, jwtSecret, "HS256", {}),
            jwt_expiration_date: tokenExpirationDate,
        };

        await User.query().update(update);

        return update;
    }

    static jsonSchema = {
        type: "object",
    };

    static relationMappings = {
        address: {
            relation: Model.HasOneThroughRelation,
            modelClass: __dirname + "/Address",
            join: {
                from: "users.id",
                through: {
                    from: "users_addresses.user_id",
                    to: "users_addresses.address_id",
                },
                to: "addresses.id",
            },
        },

        addresses: {
            relation: Model.ManyToManyRelation,
            modelClass: __dirname + "/Address",
            join: {
                from: "users.id",
                through: {
                    from: "users_addresses.user_id",
                    to: "users_addresses.address_id",
                },
                to: "addresses.id",
            },
        },

        email: {
            relation: Model.HasOneRelation,
            modelClass: __dirname + "/Email",
            join: {
                from: "users.id",
                to: "emails.owner_id",
            },
        },

        emails: {
            relation: Model.HasManyRelation,
            modelClass: __dirname + "/Email",
            join: {
                from: "users.id",
                to: "emails.owner_id",
            },
        },
    };
}
