import { Model, FilterExpression } from "objection";

export default class ModelBase extends Model {
    static defaultFilters: FilterExpression<any> = {};
    static defaultEager: string;
}
