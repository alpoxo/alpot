/*return knex.schema
        .createTable("addresses", function(table) {
            table.increments("id").primary();
            table.string("name").notNullable();
            table.string("street").notNullable();
            table.integer("plz").notNullable();
            table.string("city").notNullable();
            table.integer("state_id").references("id").inTable("states");
            table.integer("country_id").references("id").inTable("countries");
            table.timestamps();
        })*/

import { Model } from "objection";
import ModelBase from "./ModelBase";
import { Buffer } from "buffer";
import { Geometry } from "wkx";

export default class Address extends ModelBase {
    static tableName = "addresses";

    $formatDatabaseJson(json: { location?: object }) {
        if (json.location)
            json.location = Address.raw(
                `ST_GeomFromGeoJSON('${JSON.stringify(json.location)}')`,
            );

        json = super.$formatDatabaseJson(json);

        return json;
    }

    $parseDatabaseJson(json: { location?: any }) {
        json = super.$parseDatabaseJson(json);
        if (json.location)
            json.location = Geometry.parse(
                new Buffer(json.location, "hex"),
            ).toGeoJSON();

        return json;
    }

    static defaultEager = "[state, country]";

    static jsonSchema = {
        type: "object",
        required: ["street", "plz", "city"],
    };

    static relationMappings = {
        state: {
            relation: Model.BelongsToOneRelation,
            modelClass: __dirname + "/State",
            join: {
                from: "addresses.state_id",
                to: "states.id",
            },
        },

        country: {
            relation: Model.BelongsToOneRelation,
            modelClass: __dirname + "/Country",
            join: {
                from: "addresses.country_id",
                to: "countries.id",
            },
        },

        users: {
            relation: Model.ManyToManyRelation,
            modelClass: __dirname + "/User",
            join: {
                from: "addresses.id",
                through: {
                    from: "users_addresses.address_id",
                    to: "users_addresses.user_id",
                },
                to: "users.id",
            },
        },

        phones: {
            relation: Model.ManyToManyRelation,
            modelClass: __dirname + "/Phone",
            join: {
                from: "addresses.id",
                through: {
                    from: "addresses_phones.address_id",
                    to: "addresses_phones.phone_id",
                },
                to: "phones.id",
            },
        },

        emails: {
            relation: Model.ManyToManyRelation,
            modelClass: __dirname + "/Email",
            join: {
                from: "addresses.id",
                through: {
                    from: "addresses_emails.address_id",
                    to: "addresses_emails.email_id",
                },
                to: "emails.id",
            },
        },
    };
}
