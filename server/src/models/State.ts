/*
        .createTable("states", function(table) {
            table.increments("id").primary();
            table.string("state", 5).notNullable();
            table.text("description");
            table.timestamps();
        })
*/

import { Model } from "objection";
import ModelBase from "./ModelBase";

export default class State extends ModelBase {
    static tableName = "states";

    static jsonSchema = {
        type: "object",
        required: ["state"],
    };

    static relationMappings = {
        addresses: {
            relation: Model.HasManyRelation,
            modelClass: __dirname + "/Address",
            join: {
                from: "states.id",
                to: "addresses.state_id",
            },
        },
    };
}
