/*
        .createTable("emails", function(table) {
            table.increments("id").primary();
            table.string("email").notNullable();
            table.text("type");
            table.timestamps();
        })
        .createTable("addresses_emails", function(table) {
            table.integer("addresses_id").references("id").inTable("addresses");
            table.integer("emails_id").references("id").inTable("emails");
            table.primary(["addresses_id", "emails_id"]);
            table.timestamps();
        });
*/

import { Model } from "objection";
import ModelBase from "./ModelBase";

export default class Email extends ModelBase {
    static tableName = "emails";

    static jsonSchema = {
        type: "object",
        required: ["email"],

        properties: {
            id: { type: "integer" },
            email: { type: "string" },
            type: { type: "string" },
            created_at: { type: "date-time" },
            updated_at: { type: "date-time" },
        },
    };

    static relationMappings = {
        addresses: {
            relation: Model.ManyToManyRelation,
            modelClass: __dirname + "/Address",
            join: {
                from: "emails.id",
                through: {
                    from: "addresses_emails.emails_id",
                    to: "addresses_emails.address_id",
                },
                to: "addresses.id",
            },
        },
    };
}
