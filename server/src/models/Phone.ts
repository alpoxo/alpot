/*
        .createTable("phones", function(table) {
            table.increments("id").primary();
            table.string("number").notNullable();
            table.text("type");
            table.timestamps();
        })
        .createTable("addresses_phones", function(table) {
            table.integer("addresses_id").references("id").inTable("addresses");
            table.integer("phones_id").references("id").inTable("phones");
            table.primary(["addresses_id", "phones_id"]);
            table.timestamps();
        })
*/

import { Model } from "objection";
import ModelBase from "./ModelBase";

export default class Phone extends ModelBase {
    static tableName = "phones";

    static jsonSchema = {
        type: "object",
        required: ["number"],

        properties: {
            id: { type: "integer" },
            number: { type: "string" },
            type: { type: "string" },
            created_at: { type: "date-time" },
            updated_at: { type: "date-time" },
        },
    };

    static relationMappings = {
        addresses: {
            relation: Model.ManyToManyRelation,
            modelClass: __dirname + "/Address",
            join: {
                from: "phones.id",
                through: {
                    from: "addresses_phones.phones_id",
                    to: "addresses_phones.address_id",
                },
                to: "addresses.id",
            },
        },
    };
}
