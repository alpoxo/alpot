import { Model } from "objection";
import User from "./User";

const ModelMock = Object.assign(jest.fn<Model>(), {});

export const UserMock = Object.assign(jest.fn<User>(), {
    ...ModelMock,
    tableName: User.tableName,
    protectedFields: User.protectedFields,
    saveWithToken: jest.fn(),
    existsByKey: jest.fn(),
    createToken: jest.fn(() => ({
        jwt_token: "...",
        jwt_expiration_date: new Date(),
    })),
    jsonSchema: User.jsonSchema,
    relationMappings: User.relationMappings,
    query: jest.fn().mockReturnThis(),
    first: jest.fn().mockReturnThis(),
    where: jest.fn().mockReturnThis(),
    update: jest.fn().mockReturnThis(),
    insertGraph: jest.fn().mockReturnThis(),
    upsertGraph: jest.fn().mockReturnThis(),
    eager: jest.fn().mockReturnThis(),
});
