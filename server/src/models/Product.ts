/*
        .createTable("products", function(table) {
            table.increments("id").primary();
            table.string("name").notNullable();
            table.text("description").notNullable();
            table.string("picture");
            table.decimal("price").notNullable();
            table
                .integer("price_unit_id")
                .references("id")
                .inTable("price_units")
                .notNullable();
            table.specificType("location", "GEOGRAPHY(POINT, 4326)");
            table
                .integer("user_id")
                .references("id")
                .inTable("users")
                .notNullable();
            table.integer("amount").defaultTo(1);
            table.boolean("is_food");
            table.integer("states_id").references("id").inTable("states");
            table.timestamps();
        });
*/

import { Model } from "objection";
import ModelBase from "./ModelBase";

export default class Product extends ModelBase {
    static tableName = "products";

    $formatDatabaseJson(json: { location?: object }) {
        if (json.location)
            json.location = Product.raw(
                `ST_GeomFromGeoJSON(${JSON.stringify(json.location)})`,
            );

        json = super.$formatDatabaseJson(json);

        return json;
    }

    static jsonSchema = {
        type: "object",

        properties: {
            price_unit_id: { type: "integer", default: 1 },
        },
    };

    static relationMappings = {
        user: {
            relation: Model.BelongsToOneRelation,
            modelClass: __dirname + "/User",
            join: {
                from: "products.user_id",
                to: "users.id",
            },
        },

        state: {
            relation: Model.BelongsToOneRelation,
            modelClass: __dirname + "/State",
            join: {
                from: "products.state_id",
                to: "states.id",
            },
        },
    };
}
