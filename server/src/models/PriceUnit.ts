/*
        .createTable("price_units", function(table) {
            table.increments("id").primary();
            table.string("unit");
            table.text("description");
        })
*/

import ModelBase from "./ModelBase";

export default class PriceUnit extends ModelBase {
    static tableName = "price_units";

    static jsonSchema = {
        type: "object",
        required: ["unit"],

        properties: {
            id: { type: "integer" },
            unit: { type: "string" },
            description: { type: "string" },
        },
    };
}
