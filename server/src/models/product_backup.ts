import { Point } from "geojson";
import CRUD from "../util/crud";
import { pick } from "ramda";

// Please keep this in sync to
// /server/migrations/sqls/20170717120902-add-products-up.sql
export enum PRICE_UNIT {
    Hourly = "hourly",
    Amount = "amount",
}

/**
 * Defines the interface for a good
 * resource
 * 
 * @export
 * @interface User
 */
export interface Good {
    id: number;
    name: string;
    description: string;
    price: number;
    priceUnit: PRICE_UNIT;
    picture: string;
    location: Point;
    userId: number;
    createdAt?: Date;
    updatedAt?: Date;
}

/**
 * Defines the interface for a product
 * resource
 * 
 * @export
 * @interface User
 */
export interface Product extends Good {
    amount: number;
    origin: string;
    isFood: boolean;
}

const goodOnly = pick([
    "id",
    "name",
    "description",
    "price",
    "priceUnit",
    "picture",
    "location",
    "userId",
    "createdAt",
    "updatedAt",
]);

const productOnly = pick(["id", "amount", "origin", "isFood"]);

export class ProductCRUD extends CRUD<Product> {
    private goodCRUD: CRUD<Good> = new CRUD<Good>("goods");

    constructor() {
        super("products");
    }

    private async createProduct(model: Product): Promise<Product> {
        const newGoodModel = await this.goodCRUD.create(goodOnly(model));
        const newProductModel = await super.create(
            { ...productOnly(model), id: newGoodModel.id },
            false,
        );
        return {
            ...newGoodModel,
            ...newProductModel,
        };
    }

    create(model: Product): Promise<Product> {
        return this.database.tx(this.createProduct.bind(this, model));
    }

    update(model: Product): Promise<void> {
        return this.database.tx(t => {
            return t.batch([
                this.goodCRUD.update(goodOnly(model)),
                super.update(productOnly(model), false),
            ]);
        });
    }

    async find(id: number | Product): Promise<Product> {
        return this.database.tx(_ => {
            return {
                ...this.goodCRUD.find(id),
                ...super.find(id),
            };
        });
    }

    async allOfUser(username: string): Promise<Product[]> {
        const models = await this.database.any(
            `SELECT g.*, p.* FROM goods g
            NATURAL JOIN products p
            INNER JOIN users u ON g.user_id = u.id
            WHERE u.username = $1`,
            username,
        );
        return <Product[]>models.map(this.transformKeysToCamelCase);
    }

    async all(): Promise<Product[]> {
        const models = await this.database.any(
            "SELECT * FROM goods NATURAL JOIN products",
        );
        return <Product[]>models.map(this.transformKeysToCamelCase);
    }

    async delete(id: number): Promise<void> {
        await this.goodCRUD.delete(id);
        await super.delete(id);
    }
}

export const productCRUD = new ProductCRUD();
