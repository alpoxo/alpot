/**
 * The entry point for the serverside
 * of alpot
 *
 * @module util/resources.ts
 * @author Elias Bernhaut
 */

import * as Koa from "koa";
import * as bodyparser from "koa-bodyparser";
import * as cors from "koa-cors";
import "./database";
import auth from "./resources/authentication";
import users from "./resources/users";
import products from "./resources/products";
import mailVerification from "./resources/mail.verification";
import { errorHandler } from "./error";
import authentication from "./auth/authentication";

const PORT = process.env.PORT || 3000;

const app = new Koa();

app.use(
    cors({
        methods: "GET,HEAD,PUT,POST,DELETE,PATCH",
    }),
);
app.use(errorHandler);
app.use(bodyparser());
app.use(authentication.errorHandler);
app.use(auth.routes());
app.use(mailVerification.routes());
app.use(users.routes());
app.use(products.routes());

app.listen(PORT, () =>
    console.log(`Server started. Listening on port ${PORT}`),
);
