import { TransporterMock, EmailTemplateMock } from "./mailer.mock";
import defaultMailer, { Mailer, MailSendError } from "./mailer";
import { User } from "./models/user";

const transporterMock = new TransporterMock();
const mailer = new Mailer(transporterMock, EmailTemplateMock);

const user: User = {
    username: "foo",
    email: { email: "ebernhaut@itbernhaut.ch" },
};

describe("mailer", () => {
    beforeEach(() => {
        (transporterMock.sendMail as jest.Mock<{}>).mockClear();
        EmailTemplateMock.mockClear();
    });

    test("rejects if given a wrong mail", async () => {
        // Use default mailer here - it will only throw
        // a validation error
        const invalidUser = { ...user, email: "invalid_mail" };

        const mailOptions = {
            html: "test html",
        };

        const sendMailPromise = defaultMailer.sendMail(
            mailOptions,
            invalidUser,
        );
        const expectRejectedSend = await expect(sendMailPromise).rejects;

        expectRejectedSend.toBeInstanceOf(MailSendError);
        expectRejectedSend.toMatchObject({
            users: [invalidUser],
            mailOptions,
        });
    });

    test("rejects if a wrong template is given to sendMailTemplate", async () => {
        EmailTemplateMock.mockImplementation((template: string) => {
            throw new Error("no such file or directory");
        });

        await expect(
            mailer.sendMailTemplate("invalid_template", {}, user),
        ).rejects.toBeDefined();
    });

    test("can send individual messages to every templated user", async () => {
        const users: User[] = [
            { username: "foo1", email: "bar@test.com" },
            { username: "foo2", email: "foobar@test.com" },
        ];

        EmailTemplateMock.mockImplementation(() => ({
            render: (context: any) => ({ html: "you got your html" }),
        }));

        const expectResponse = await expect(
            mailer.sendMailTemplate("registration", {}, ...users),
        ).resolves.toBeUndefined();

        expect(transporterMock.sendMail).toHaveBeenCalledTimes(2);
    });

    test("injects additional options when passing them to sendMailtemplate", async () => {
        const additionalOptions = {
            foo: "bar",
            any: "thing",
        };

        EmailTemplateMock.mockImplementation(() => ({
            render: (context: any) => {
                expect(context).toMatchObject(additionalOptions);
                return {}; // We have to return something
            },
        }));

        await mailer.sendMailTemplate("registration", additionalOptions, user);
    });

    test("injects environment variables to templates by default", async () => {
        EmailTemplateMock.mockImplementation(() => ({
            render: (context: any) => {
                expect(context.env).toBe(process.env);
                return {}; // We have to return something
            },
        }));

        await mailer.sendMailTemplate("registration", {}, user);
    });

    test("sends a mail to a single user", async () => {
        const options = {
            html: "mailer test",
        };
        await mailer.sendMail(options, user);
        expect(transporterMock.sendMail).toBeCalledWith(
            expect.objectContaining({
                ...options,
                to: user.email.email,
                from: `${process.env.MAIL_NAME} <${process.env.MAIL_FROM}>`,
            }),
        );
    });
});
