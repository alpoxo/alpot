/**
 * Handles the authentication process of
 * alpot
 * @module auth/authentication.ts
 * @author Elias Bernhaut
 */

import {
    jwtSecret,
    emailVerificationRoute,
    HASH_ITERATIONS,
    HASH_EMAIL_LENGTH,
    HASH_PASSWORD_LENGTH,
    HASH_PASSWORD_SALT,
} from "../config/configuration";
import * as jwtKoa from "koa-jwt";
import * as Koa from "koa";
import UserModel from "../models/User";
import defaultMailer, { Mailer, MailSendError } from "../mailer";
import * as crypto from "crypto";
import { omit } from "ramda";

const BAD_REQUEST = 400;
const UNAUTHORIZED = 401;
const INTERNAL_SERVER_ERROR = 500;

/**
 * Handles the authentication process
 * 
 * @export
 * @class Authentication
 */
export class Authentication {
    /**
     * Koa middleware for authentication through a jwt token
     */
    authenticate: Koa.Middleware = jwtKoa({ secret: jwtSecret });

    constructor(private User: typeof UserModel, private mailer: Mailer) {}

    /**
     * Creates a random verification key from the email
     * 
     * @param email The email which has to be verified.
     */
    private generateMailVerificationKey(email: string): Promise<string> {
        // Generate email hash with 10k iterations
        const salt = crypto.randomBytes(128).toString("base64");

        // Encrypt the mail
        return this.encryptString(
            email,
            salt,
            HASH_ITERATIONS,
            HASH_EMAIL_LENGTH,
        );
    }

    /**
     * Encrypt some string with the crypto
     * pdkdf2 algorithm.
     * 
     * @param str The string to encrypt
     * @param salt The salt
     * @param iterations How many key iterations should
     *  be done to generate the key
     * @param length How long should the encrypted
     *  value be
     */
    encryptString = (
        str: string,
        salt: string,
        iterations: number,
        length: number,
    ): Promise<string> =>
        new Promise((resolve, reject) =>
            crypto.pbkdf2(
                str,
                salt,
                iterations,
                length,
                "sha512",
                (err, key) => {
                    if (err) reject(err);
                    resolve(key.toString("hex"));
                },
            ),
        );

    /**
     * Koa middleware for handling unauthorized access
     *
     * @param {Koa.Middleware} ctx - The koa context
     * @param {() => Promise<any>)} next - Calls the next middleware
     * @returns {Promise<any>} - A promise which waits for the completion of
     *                          the middleware
     */
    errorHandler: Koa.Middleware = async (ctx, next) => {
        try {
            return await next();
        } catch (err) {
            // Rethrow error if not 401
            if (err.status !== UNAUTHORIZED) throw err;
            ctx.status = UNAUTHORIZED;
            ctx.body =
                "You are trying to access a protected resource. " +
                    "You don't have the rights to access it." || err.message;
        }
    };

    /**
     * Verifies the mail of a user
     * 
     * @type {Koa.Middleware}
     * @memberof Authentication
     */
    verifyMail: Koa.Middleware = async (ctx, next) => {
        const verification_key = ctx.params && ctx.params.verification_key;

        if (!verification_key)
            return ctx.throw(BAD_REQUEST, "invalid-verification-key");

        const user = await this.User
            .query()
            .where({ verification_key })
            .first();

        if (!user) return ctx.throw(BAD_REQUEST, "invalid-verification-key");

        if (user.verified)
            return ctx.throw(BAD_REQUEST, "already-verified-key");

        await this.User
            .query()
            .update({ verified: true } as any)
            .where("id", user.id);

        ctx.body = "You successfully validated your email address.";
        ctx.status = 200;

        return next();
    };

    /**
     * Koa middleware for registration
     *
     * @param {Koa.Middleware} ctx - The koa context
     * @param {() => Promise<any>)} next - Calls the next middleware
     * @returns {Promise<any>} - A promise which waits for the completion of
     *                          the middleware
     */
    registration: Koa.Middleware = async (ctx, next) => {
        if (!ctx.request.body)
            throw new Error(
                "The context body was undefined. Please go " +
                    "sure to use bodyparser.",
            );

        let user;
        const {
            username,
            password,
            email: { email } = { email: null },
        }: {
            username: string;
            password: string;
            email: { email: string };
        } = (user = ctx.request.body);

        if (!username || !password || !email)
            return ctx.throw(BAD_REQUEST, "server-registration");

        // Ask the database for the existance of the
        // key/value pairs of email and password
        const exists = await this.User.existsByKey({
            username,
            "email.email": email,
        });

        // Throw verification errors for each field
        // If this key/value pair already exists in the database
        if (exists.username || exists.email) {
            const usernameError = "server-registration-username-exists";
            const emailError = "server-registration-email-exists";

            return ctx.throw(BAD_REQUEST, {
                fields: {
                    username: exists.username ? usernameError : undefined,
                    email: exists.email ? emailError : undefined,
                },
            });
        }

        try {
            user.verification_key = await this.generateMailVerificationKey(
                email,
            );

            user.password = await this.encryptString(
                user.password,
                HASH_PASSWORD_SALT,
                HASH_ITERATIONS,
                HASH_PASSWORD_LENGTH,
            );

            // Try to send a mail to the user.
            // the registration will not be completed when we don't
            // get a successful send!
            await this.mailer.sendMailTemplate(
                "registration",
                {
                    emailVerificationRoute,
                },
                user,
            );

            // Set the type of the email which we store
            user.email.type = "main";

            // Finally save to database
            await this.User.query().insertGraph(user);
        } catch (e) {
            console.error(e);
            // If we got an error from sending the mail
            // We catch it here
            if (e instanceof MailSendError)
                return ctx.throw(
                    INTERNAL_SERVER_ERROR,
                    "server-registration-email-send-error",
                );

            // This should never happen. Hopefully.
            return ctx.throw(
                INTERNAL_SERVER_ERROR,
                "server-registration-unexpected-error",
            );
        }

        ctx.body = {};
        ctx.status = 200;

        return next();
    };

    /**
     * Login middleware for koajs
     *
     * @param {Koa.Middleware} ctx - The koa context
     * @param {() => Promise<any>)} next - Calls the next middleware
     * @returns {Promise<an>} - A promise which waits for the completion of
     *                          the middleware
     */
    login: Koa.Middleware = async (ctx, next) => {
        const { username, password } = ctx.request.body;

        // Throw bad request error if the
        // username or the password is not supplied
        if (!username || !password)
            return ctx.throw(BAD_REQUEST, "server-login-input-missing");

        // Get the requested user from the database
        const user = await this.User.query().where({ username }).first();

        // Throw bad request error if the user does not exist
        if (!user)
            return ctx.throw(BAD_REQUEST, "server-login-username-not-exists");

        // Throw bad request error if the user did not yet validate
        // his email address
        if (!user.verified)
            return ctx.throw(BAD_REQUEST, "server-login-verification");

        // Encrypt password for comparison
        const encryptedPassword = await this.encryptString(
            password,
            HASH_PASSWORD_SALT,
            HASH_ITERATIONS,
            HASH_PASSWORD_LENGTH,
        );

        // Throw bad request error if the passwords don't match
        // The password is supposed to be encrypted with bcrypt
        if (!user || user.password !== encryptedPassword)
            return ctx.throw(BAD_REQUEST, "server-login-password-mismatch");

        // Return the previously saved token - if any.
        let tokenInformation = {
            jwt_token: user.jwt_token,
            jwt_expiration_date: user.jwt_expiration_date,
        };

        if (
            !tokenInformation.jwt_token ||
            (tokenInformation.jwt_expiration_date &&
                tokenInformation.jwt_expiration_date < new Date())
        )
            tokenInformation = await this.User.createToken(user);

        ctx.body = {
            token: tokenInformation.jwt_token,
            expiration_date: tokenInformation.jwt_expiration_date,
        };
        ctx.user = user;
        ctx.status = 200;

        return next();
    };

    /**
     * Gets the currently authenticated user
     *
     * @param {Koa.Middleware} ctx - The koa context
     * @param {() => Promise<any>)} next - Calls the next middleware
     * @returns {Promise<an>} - A promise which waits for the completion of
     *                          the middleware
     */
    getCurrentUser: Koa.Middleware = async (ctx, next) => {
        // The headers in koa are all delivered in lower-case
        const authHeader = ctx.request.headers["authorization"];
        if (!authHeader) return ctx.throw(400, "server-user-not-logged-in");

        const jwt_token = authHeader.replace(/^Bearer /, "");

        const user = await this.User
            .query()
            .eager(this.User.defaultEager, this.User.defaultFilters)
            .where({ jwt_token })
            .first();

        if (!user) return ctx.throw(400, "server-user-not-found");

        ctx.body = omit(this.User.protectedFields, user);
        ctx.status = 200;

        await next();
    };
}

/**
 * By default, export the default authentication
 */
export default new Authentication(UserModel, defaultMailer);
