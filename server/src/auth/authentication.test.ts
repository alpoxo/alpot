import { Authentication } from "./authentication";
import { Context } from "koa";
import { MailerMock } from "../mailer.mock";
import { UserMock } from "../models/mocks";
import { emailVerificationRoute } from "../config/configuration";
import {
    HASH_EMAIL_LENGTH,
    HASH_PASSWORD_LENGTH,
    HASH_PASSWORD_SALT,
    HASH_ITERATIONS,
} from "../config/configuration";

const ContextMock = jest.fn<Context>(() => ({
    throw: jest.fn(),
    request: {
        headers: [],
    },
    body: {},
    headers: [],
}));

const mailerMock = new MailerMock();
const {
    registration,
    login,
    verifyMail,
    encryptString,
    getCurrentUser,
} = new Authentication(UserMock as any, mailerMock);

UserMock.existsByKey.mockImplementation(() => ({
    username: false,
    "emails.email": false,
}));

beforeEach(() => {
    UserMock.mockClear();
    for (let key in UserMock) {
        const value = (UserMock as any)[key];
        if (value && value.mock) value.mockClear();
    }
});

describe("verifyMail", () => {
    test("throws bad request if no validation key is given", async () => {
        const contextMock = new ContextMock();
        await verifyMail(contextMock, jest.fn());
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("throws if an invalid validation key is given", async () => {
        const contextMock = new ContextMock();
        contextMock.params = { verification_key: "invalidkey" };

        UserMock.first.mockImplementationOnce(() => null);

        await verifyMail(contextMock, jest.fn());
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("verifies if the validation key is valid", async () => {
        const contextMock = new ContextMock();
        const verification_key = "mySpecialKey";
        const nextMock = jest.fn();

        contextMock.params = { verification_key };

        UserMock.first.mockImplementationOnce(() => ({
            verification_key,
        }));

        await verifyMail(contextMock, nextMock);

        expect(UserMock.update).toBeCalledWith(
            expect.objectContaining({
                verified: true,
            }),
        );
        expect(nextMock).toBeCalled();
        expect(contextMock.throw).not.toBeCalled();
        expect(contextMock.status).toBe(200);
    });
});

describe("registration", () => {
    test("throws if no body given.", async () => {
        const contextMock = new ContextMock();
        await expect(
            registration(contextMock, () => Promise.resolve()),
        ).rejects.toBeDefined();
    });

    test("throws bad request if username is missing.", async () => {
        const nextMock = jest.fn();
        const contextMock = new ContextMock();
        contextMock.request.body = { email: "foo", password: "bar" };
        await registration(contextMock, nextMock);
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("throws bad request if password is missing.", async () => {
        const nextMock = jest.fn();
        const contextMock = new ContextMock();
        contextMock.request.body = { username: "foo", email: "bar" };
        await registration(contextMock, nextMock);
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("throws bad request if email is missing.", async () => {
        const nextMock = jest.fn();
        const contextMock = new ContextMock();
        contextMock.request.body = { username: "foo", password: "bar" };
        await registration(contextMock, nextMock);
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("creates a user", async () => {
        const nextMock = jest.fn();
        const contextMock = new ContextMock();
        contextMock.request.body = {
            username: "foo",
            password: "bar",
            email: { email: "foobar", type: "main" },
        };

        await registration(contextMock, nextMock);
        expect(contextMock.throw).not.toBeCalled();
        expect(nextMock).toBeCalled();
        expect(UserMock.insertGraph).toBeCalled();
    });

    test("created user has email verification key", async () => {
        const contextMock = new ContextMock();
        contextMock.request.body = {
            username: "foo",
            password: "bar",
            email: { email: "foobar" },
        };

        await registration(contextMock, () => void {});
        expect(contextMock.throw).not.toBeCalled();
        expect(UserMock.insertGraph).toHaveBeenCalledWith(
            expect.objectContaining({
                verification_key: expect.any(String),
            }),
        );

        const expectSavedKey = expect(
            UserMock.insertGraph.mock.calls[0][0].verification_key,
        );
        expectSavedKey.not.toBe("bar");
        expectSavedKey.toHaveLength(HASH_EMAIL_LENGTH * 2);
    });

    test("created user has an encrypted password", async () => {
        const contextMock = new ContextMock();
        contextMock.request.body = {
            username: "foo",
            password: "bar",
            email: { email: "foobar" },
        };
        await registration(contextMock, () => void {});
        const expectPassword = expect(
            UserMock.insertGraph.mock.calls[0][0].password,
        );
        expectPassword.not.toBe("bar");
        expectPassword.toHaveLength(HASH_PASSWORD_LENGTH * 2);
    });

    test("calls next on success", async () => {
        const nextMock = jest.fn();
        const contextMock = new ContextMock();
        contextMock.request.body = {
            username: "foo",
            password: "bar",
            email: { email: "foobar" },
        };
        (mailerMock.sendMailTemplate as jest.Mock<{}>).mockClear();
        await registration(contextMock, nextMock);
        expect(contextMock.throw).not.toBeCalled();
        expect(UserMock.insertGraph).toBeCalled();
        expect(nextMock).toBeCalled();
        expect(mailerMock.sendMailTemplate).toBeCalled();
        expect(contextMock.status).toBe(200);
    });

    test("sends a valid mail after successful registration", async () => {
        const contextMock = new ContextMock();
        contextMock.request.body = {
            username: "foo",
            password: "bar",
            email: { email: "foobar" },
        };
        await registration(contextMock, jest.fn());
        expect(mailerMock.sendMailTemplate).toHaveBeenCalledWith(
            "registration",
            expect.objectContaining({ emailVerificationRoute }),
            expect.objectContaining({
                ...contextMock.request.body,
                verification_key: expect.any(String),
            }),
        );
    });
});

describe("login", () => {
    test("throws bad request if username is missing", async () => {
        const contextMock = new ContextMock();
        contextMock.request.body = { password: "foo" };
        await login(contextMock, jest.fn());
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("throws bad request if password is missing", async () => {
        const contextMock = new ContextMock();
        contextMock.request.body = { username: "foo" };
        await login(contextMock, jest.fn());
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("throws bad request if the user does not exist", async () => {
        const contextMock = new ContextMock();
        contextMock.request.body = { username: "foo", password: "foobar" };

        UserMock.first.mockImplementationOnce((): any => null);

        await login(contextMock, jest.fn());
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("throws bad request if password does not match", async () => {
        const contextMock = new ContextMock();
        contextMock.request.body = { username: "foo", password: "bar" };

        UserMock.first.mockImplementationOnce((): any => null);

        await registration(contextMock, jest.fn());
        contextMock.request.body = { username: "foo", password: "foobar" };
        await login(contextMock, jest.fn());
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("cannot login if the email of the user is not verified", async () => {
        const contextMock = new ContextMock();
        const nextMock = jest.fn();
        contextMock.request.body = {
            username: "foo",
            password: "bar",
        };

        UserMock.first.mockImplementationOnce(() => ({
            ...contextMock.request.body,
            verified: false,
        }));

        await login(contextMock, nextMock);
        expect(nextMock).not.toBeCalled();
        expect(contextMock.throw).toBeCalledWith(400, expect.any(String));
    });

    test("is successful on valid credentials", async () => {
        const nextMock = jest.fn();
        const contextMock = new ContextMock();
        contextMock.request.body = {
            username: "foo",
            email: { email: "foobar" },
            password: "bar",
        };
        const encryptedPassword = await encryptString(
            contextMock.request.body.password,
            HASH_PASSWORD_SALT,
            HASH_ITERATIONS,
            HASH_PASSWORD_LENGTH,
        );

        UserMock.first.mockImplementationOnce(() => ({
            ...{ ...contextMock.request.body, password: encryptedPassword },
            verified: true,
        }));

        await login(contextMock, nextMock);
        expect(contextMock.body).toMatchObject({
            token: expect.any(String),
            expiration_date: expect.any(Date),
        });
        expect(UserMock.createToken).toBeCalled();
        expect(contextMock.user).toBeDefined();
        expect(nextMock).toBeCalled();
        expect(contextMock.status).toBe(200);
    });
});

describe("getCurrentUser", () => {
    test("throws bad request if no authorization header given", async () => {
        const contextMock = new ContextMock();
        await getCurrentUser(contextMock, jest.fn());
        expect(contextMock.throw).toHaveBeenCalledWith(400, expect.any(String));
    });

    test("throws bad request if the authorization header is invalid", async () => {
        const contextMock = new ContextMock();
        contextMock.request.headers["authorization"] = "invalid_header";
        UserMock.first.mockImplementationOnce((): any => null);
        await getCurrentUser(contextMock, jest.fn());
        expect(contextMock.throw).toHaveBeenCalledWith(400, expect.any(String));
    });

    test("returns user if authorization header is valid", async () => {
        const contextMock = new ContextMock();
        const nextMock = jest.fn();

        UserMock.first.mockImplementationOnce(() => ({
            username: "valid_user",
        }));

        contextMock.request.headers["authorization"] = "valid_token";
        await getCurrentUser(contextMock, nextMock);
        expect(contextMock.body).toMatchObject({
            username: "valid_user",
        });
        expect(contextMock.status).toBe(200);
        expect(nextMock).toHaveBeenCalled();
    });
});
