/**
 * The configuration of alpot
 *
 * @module config/configuration.ts
 * @author Elias Bernhaut
 */

export const createTokenExpiration = () => {
    const date = new Date();
    date.setDate(date.getDate() + jwtExpirationDuration);
    return date;
};
export const jwtExpirationDuration = 30;
export const jwtSecret = "MyS3cr3tK3Y";
export const jwtSession = {
    session: false,
};
export const emailVerificationRoute = "email-verification";

export const HASH_EMAIL_LENGTH = 15;
export const HASH_PASSWORD_SALT =
    "EPGOIUP29Z03U'0iuqpizsp08zpiho9zr0'hoiu2äo8zg";
export const HASH_PASSWORD_LENGTH = 20;
export const HASH_ITERATIONS = 10000;
