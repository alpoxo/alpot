// Update with your config settings.

module.exports = {
    development: {
        client: "pg",
        connection: {
            database: "alpot",
            user: "elias",
            password: "postgres",
        },
    },

    testing: {
        client: "sqlite3",
        useNullAsDefault: true,
        connection: {
            filename: ":memory:",
        },
    },
};
